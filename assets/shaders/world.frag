uniform sampler2D source;
uniform sampler2D lightmap;
uniform sampler2D normalmap;
uniform float factor;
uniform float daylight;

varying vec4 relativePosition;

vec4 getComponents(vec4 normal) {
	vec2 rot = normal.xy*2.0 - 1.0;
	return vec4(
		clamp(-rot.y, 0.0, 1.0),
		clamp(rot.x, 0.0, 1.0),
		clamp(rot.y, 0.0, 1.0),
		clamp(-rot.x, 0.0, 1.0)
	);
}

vec4 mix3(vec4 x, vec4 y, vec4 z, float a) {
	if (a < 0.5) {
		return mix(x, y, 2.0 * a);
	} else {
		return mix(y, z, 2.0 * a - 1.0);
	}
}

vec4 calcLight(vec2 rel, vec4 normal, vec4 rot) {
	vec4 lightCur   = texture2D(lightmap, rel);
	vec4 lightAbove = lightCur;
	vec4 lightUp    = texture2D(lightmap, rel + vec2(0.0, -1.0) / 18.0);
	vec4 lightRight = texture2D(lightmap, rel + vec2(1.0, 0.0) / 18.0);
	vec4 lightDown  = texture2D(lightmap, rel + vec2(0.0, 1.0) / 18.0);
	vec4 lightLeft  = texture2D(lightmap, rel + vec2(-1.0, 0.0) / 18.0);

	lightAbove = max(lightAbove, min((lightUp + lightRight + lightDown + lightLeft) / 5.0, 0.6));
	vec4 lightV = rot[0]*lightUp + rot[1]*lightRight + rot[2]*lightDown + rot[3]*lightLeft + (normal.z*2.0 - 1.0)*lightAbove;

	return lightV;
}

void main() {
	const vec4 BLUE = vec4(0, 0.75, 1.0, 1.0);
	vec4 color = texture2D(source, gl_TexCoord[0].xy);
	vec4 normal = texture2D(normalmap, gl_TexCoord[0].xy);
	vec4 rot = getComponents(normal);

	vec2 rel = (floor(relativePosition.xy / 64.0 + 1.0) + 0.01) / 18.0;

	float artificial = calcLight(rel, normal, rot)[0];
	float sunlight = calcLight(rel + vec2(0.0, 0.5) / 18.0, normal, rot)[0];
	float light = min(sunlight * (0.8 * daylight + 0.2) + artificial, 1.0);

	if (color[3] < 0.1) {
		gl_FragColor = color;
	} else {
		float u = 1.0 - clamp(factor, 0.0, 1.0);
		gl_FragColor = mix(BLUE, color, u) * u * u * light;
		gl_FragColor[3] = color[3];
	}
}
