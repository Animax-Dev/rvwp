local ChatCommand = commands.ChatCommand

rvwp.register_chat_command("tile", ChatCommand:new({
	func = function(self, name, param)
		local pos = rvwp.get_player_entity(name):get_pos()
		return true, dump(rvwp.get_tile(pos))
	end
}))

rvwp.register_chat_command("place", ChatCommand:new({
	func = function(self, name, param)
		local pos = rvwp.get_player_entity(name):get_pos()

		rvwp.set_tile(pos, {
			name = "wall"
		})
	end
}))

rvwp.register_chat_command("findmeta", ChatCommand:new({
	func = function(self, name, param)
		local entity = rvwp.get_player_entity(name)

		local poses, values = rvwp.find_with_metadata(entity:get_pos(), 5, rvwp.layers.tile, "job_giver")

		return true, dump(poses) .. "\n" .. dump(values)
	end
}))

rvwp.register_chat_command("meta", ChatCommand:new({
	func = function(self, name, param)
		local entity = rvwp.get_player_entity(name)
		local entityMeta = entity:get_meta()

		local counter = entityMeta:get_int("counter")
		entityMeta:set_int("counter", (counter or 0) + 1)

		local tileMeta = rvwp.get_meta(entity:get_pos(), rvwp.layers.tile)
		local counter2
		if tileMeta then
			counter2 = tileMeta:get_int("counter")
			tileMeta:set_int("counter", (counter2 or 0) + 1)
			tileMeta:set_bool("job_giver", true)
		else
			counter2 = "not loaded"
		end

		return true, dump(counter) .. "\n" .. dump(counter2)
	end
}))

rvwp.register_chat_command("timer", ChatCommand:new({
	func = function(self, name, param)
		local pos = rvwp.get_player_entity(name):get_pos()

		local timer = rvwp.get_timer(pos, rvwp.layers.tile, "myevent")
		if not timer then
			return false, "Unable to get timer - is the block loaded?"
		end

		if not timer:start(5) then
			return false, "Failed to start"
		end
		return true, "Started timer"
	end
}))

rvwp.register_chat_command("stove", commands.ChatCommand:new({
	func = function(self, name, param)
		local player_pos = rvwp.get_player_entity(name):get_pos()
		local poses, _ = rvwp.find_with_metadata(player_pos, 50, rvwp.layers.tile, "job_giver")

		local started = 0
		for _, pos in pairs(poses) do
			local timer = rvwp.get_timer(pos, rvwp.layers.tile, "cook")
			if timer and timer:start(5) then
				started = started + 1
			end
		end

		return true, ("stove: %d timers started, out of %d tiles"):format(started, #poses)
	end
}))
