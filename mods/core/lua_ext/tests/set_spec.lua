_G.class = {}
_G.Set = {}

dofile("mods/core/lua_ext/table.lua")
dofile("mods/core/lua_ext/class.lua")
dofile("mods/core/lua_ext/Set.lua")

local say = require("say")

local function same_elements(state, arguments)
	local elements = {}

	if type(arguments[1]) ~= "table" or type(arguments[2]) ~= "table" or
			#arguments ~= 2 then
		return false
	end

	for _, value in pairs(arguments[1]) do
		elements[value] = (elements[value] or 0) + 1
	end

	for _, value in pairs(arguments[2]) do
		local count = elements[value] or 0
		if count <= 0 then
			return false
		end
		elements[value] = count - 1
	end

	for _, value in pairs(elements) do
		if value > 0 then
			return false
		end
	end

	return true
end

say:set("assertion.same_elements.positive", "Expected %s \nto have elements: %s")
say:set("assertion.same_elements.negative", "Expected %s \nto not have same elements: %s")
assert:register("assertion", "same_elements", same_elements,
	"assertion.same_elements.positive", "assertion.same_elements.negative")

describe("Set", function()
	describe("new", function()
		it("empty", function()
			local set = Set:new()
			assert.is_nil(next(set.data))
			assert.same_elements({}, set:to_table())
		end)

		it("items", function()
			local set = Set:new({ 1, 3 })
			assert.same_elements({1, 3}, set:to_table())
		end)

		it("copy", function()
			local function hasher(x)
				return x + 1
			end

			local set = Set:new({ 1, 3 }, hasher)
			local copy = Set:new(set)
			assert.equal(hasher, copy.hasher)
			copy:add(4)
			assert.same_elements({1, 3, 4}, copy:to_table())
			assert.same_elements({1, 3}, set:to_table())
		end)
	end)

	it("equals", function()
		local set = Set:new()
		assert.is_true(set == Set:new())
		assert.same_elements({}, set:to_table())
	end)

	it("add remove", function()
		local set = Set:new()
		assert.same_elements({}, set:to_table())
		set:add(2)
		assert.same_elements({2}, set:to_table())
		set:add_all({ 1, 3 })
		assert.same_elements({1, 2, 3}, set:to_table())
		set:remove(2)
		assert.same_elements({1, 3}, set:to_table())
	end)

	it("union", function()
		local set1 = Set:new({ 1, 2, 3 })
		local set2 = Set:new({ 3, 4, 5 })
		local set3 = set1 + set2

		assert.same_elements({1, 2, 3, 4, 5}, set3:to_table())
	end)

	it("difference", function()
		local set1 = Set:new({ 1, 2, 3 })
		local set2 = Set:new({ 3, 4, 5 })
		local set3 = set1 - set2

		assert.same_elements({1, 2}, set3:to_table())
	end)

	it("intersection", function()
		local set1 = Set:new({ 1, 2, 3 })
		local set2 = Set:new({ 3, 4, 5 })
		local set3 = set1:intersection(set2)

		assert.same_elements({3}, set3:to_table())
	end)

	it("is_subset", function()
		local set1 = Set:new({ 1, 2, 3 })
		local set2 = Set:new({ 3, 4, 5 })
		assert.is_false(set1:is_subset(set2))
		local set3 = Set:new({ 2, 3 })
		assert.is_true(set1:is_subset(set3))
	end)

	it("is_disjoint", function()
		local set1 = Set:new({ 1, 2, 3 })
		local set2 = Set:new({ 3, 4, 5 })
		assert.is_false(set1:is_disjoint(set2))
		local set3 = Set:new({ 6, 7 })
		assert.is_true(set1:is_disjoint(set3))
	end)

end)
