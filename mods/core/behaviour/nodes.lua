local parse_variable = behaviour.parse_variable
local get_position = behaviour.get_position
local get_value = behaviour.get_value
local states = behaviour.states


--
-- Root node
--

behaviour.register_factory("root", function(args, children, blackboard)
	assert(#children == 1)
	return children[1]
end)


--
-- Decorators
--
behaviour.make_decorator("failure", function(self, ...)
	if self.children[1]:run(...) ~= states.RUNNING then
		return states.FAILED
	end
end)

behaviour.make_decorator("success", function(self, ...)
	if self.children[1]:run(...) ~= states.RUNNING then
		return states.SUCCESS
	end
end)

behaviour.make_decorator("invert", function(self, ...)
	local state = self.children[1]:run(...)
	if state == states.FAILED then
		return states.SUCCESS
	elseif state == states.SUCCESS then
		return states.SUCCESS
	else
		return state
	end
end)

behaviour.make_decorator("Exists", function(self, ...)
	local variable = assert(self.args.field)
	if not self.blackboard[variable] then
		return states.FAILED
	end

	return self.children[1]:run(...)
end)

behaviour.make_decorator("GetNearPlayerPosition", function(self, ...)
	local args = self.args
	local variable = assert(parse_variable(args.field))
	local range = tonumber(args.range)

	local entities = rvwp.get_entities_in_range(self.object.entity:get_pos(), range)
	for i=1, #entities do
		if entities[i]:get_type() == "Player" then
			self.blackboard[variable] = entities[i]:get_pos()
			return self.children[1]:run(...)
		end
	end

	return states.FAILED
end)


--
-- Actions
--

behaviour.make_node("GoTo", function(self)
	assert(#self.children == 0)

	local f = rvwp.entity.actions.go_to

	local target = get_position(self.blackboard, self.args.target)
	if not target then
		return states.FAILED
	end

	local action = self.object:set_action_if_not_set(f, target)
	if action:is_complete() then
		if action:is_successful() then
			return states.SUCCESS
		else
			return states.FAILED
		end
	else
		return states.RUNNING
	end
end)

behaviour.make_node("GoToNeighbour", function(self)
	assert(#self.children == 0)

	local f = rvwp.entity.actions.go_to_neighbour

	local target = assert(get_position(self.blackboard, self.args.target))
	local accept_below_raw = get_value(self.blackboard, self.args.acceptbelow)
	assert(accept_below_raw ~= nil)
	local accept_below = string.is_yes(accept_below_raw)

	local action = self.object:set_action_if_not_set(f, target, accept_below)
	if action:is_complete() then
		if action:is_successful() then
			return states.SUCCESS
		else
			return states.FAILED
		end
	else
		return states.RUNNING
	end
end)

behaviour.make_node("GetRandomLocation", function(self)
	local variable = assert(parse_variable(self.args.field))
	while true do
		local pos = V(math.random(-100, 100), math.random(-100, 100), 0)
		local surface = rvwp.get_surface_height(pos)
		if surface then
			pos.z = surface
			self.blackboard[variable] = pos
			return states.SUCCESS
		end
	end
end)
