_G.behaviour = {}
_G.class = {}

dofile("mods/core/behaviour/api.lua")
dofile("mods/core/lua_ext/class.lua")
dofile("mods/core/lua_ext/table.lua")
dofile("mods/core/behaviour/bt.lua")

local mocks = dofile("mods/core/behaviour/tests/mocks.lua")

local MockBehaviour = mocks.MockBehaviour
local faketree = {
	on_enter = function() end,
	on_exit = function() end,
}
local fakeobject = {
	set_action = 3,
}

describe("FickleSequence", function()
	local FickleSequence = behaviour.FickleSequence
	assert(FickleSequence)

	it("runs in order", function()
		local a = MockBehaviour:new()
		local b = MockBehaviour:new()

		local function reset()
			a.ran = false
			b.ran = false
		end

		local seq = FickleSequence:new({a, b})
		seq:attach(faketree, fakeobject)
		assert.equals("running", seq:get_state())

		assert.is_false(a.ran)
		assert.is_false(b.ran)
		seq:run(0.1)
		assert.is_true(a.ran)
		assert.is_false(b.ran)
		assert.equals("running", seq:get_state())

		reset()
		seq:run(0.1)
		assert.is_true(a.ran)
		assert.is_false(b.ran)
		assert.equals("running", seq:get_state())

		a.result = "success"

		reset()
		seq:run(0.1)
		assert.is_true(a.ran)
		assert.is_true(b.ran)
		assert.equals("running", seq:get_state())

		reset()
		seq:run(0.1)
		assert.is_true(a.ran)
		assert.is_true(b.ran)
		assert.equals("running", seq:get_state())

		b.result = "success"

		reset()
		seq:run(0.1)
		assert.is_true(a.ran)
		assert.is_true(b.ran)
		assert.equals("success", seq:get_state())
	end)

	it("handles failed", function()
		local a = MockBehaviour:new()
		local b = MockBehaviour:new()
		a.result = "success"

		local function reset()
			a.ran = false
			b.ran = false
		end

		local seq = FickleSequence:new({a, b})
		seq:attach(faketree, fakeobject)
		assert.equals("running", seq:get_state())

		assert.is_false(a.ran)
		assert.is_false(b.ran)
		seq:run(0.1)
		assert.is_true(a.ran)
		assert.is_true(b.ran)
		assert.equals("running", seq:get_state())

		b.result = "failed"

		reset()
		seq:run(0.1)
		assert.is_true(a.ran)
		assert.is_true(b.ran)
		assert.equals("failed", seq:get_state())
	end)
end)


describe("Sequence", function()
	local Sequence = behaviour.Sequence
	assert(Sequence)

	it("runs in order", function()
		local a = MockBehaviour:new()
		local b = MockBehaviour:new()

		local function reset()
			a.ran = false
			b.ran = false
		end

		local seq = Sequence:new({a, b})
		seq:attach(faketree, fakeobject)

		assert.is_false(a.ran)
		assert.is_false(b.ran)
		seq:run(0.1)
		assert.is_true(a.ran)
		assert.is_false(b.ran)
		assert.equals("running", seq:get_state())

		reset()
		seq:run(0.1)
		assert.is_true(a.ran)
		assert.is_false(b.ran)
		assert.equals("running", seq:get_state())

		a.result = "success"

		reset()
		seq:run(0.1)
		assert.is_true(a.ran)
		assert.is_true(b.ran)
		assert.equals("running", seq:get_state())

		reset()
		seq:run(0.1)
		assert.is_false(a.ran)
		assert.is_true(b.ran)
		assert.equals("running", seq:get_state())

		b.result = "success"

		reset()
		seq:run(0.1)
		assert.is_false(a.ran)
		assert.is_true(b.ran)
		assert.equals("success", seq:get_state())
	end)


	it("handles failed", function()
		local a = MockBehaviour:new()
		local b = MockBehaviour:new()
		a.result = "success"

		local function reset()
			a.ran = false
			b.ran = false
		end

		local seq = Sequence:new({a, b})
		seq:attach(faketree, fakeobject)

		assert.is_false(a.ran)
		assert.is_false(b.ran)
		seq:run(0.1)
		assert.is_true(a.ran)
		assert.is_true(b.ran)
		assert.equals("running", seq:get_state())

		reset()
		seq:run(0.1)
		assert.is_false(a.ran)
		assert.is_true(b.ran)
		assert.equals("running", seq:get_state())

		b.result = "failed"

		reset()
		seq:run(0.1)
		assert.is_false(a.ran)
		assert.is_true(b.ran)
		assert.equals("failed", seq:get_state())
	end)
end)
