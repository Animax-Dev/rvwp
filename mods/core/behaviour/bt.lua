local states = table.enum({
	RUNNING = "running",
	FAILED = "failed",
	SUCCESS = "success",
})


behaviour.states = states

local function debug(...) -- luacheck: ignore
	-- minetest.log("warning", ...)
end


local Node = class()
behaviour.Node = Node

function Node:attach(tree, object)
	assert(tree and tree.on_enter and tree.on_exit)
	self.tree = tree

	assert(object.set_action)
	self.object = object

	if self.children then
		for i=1, #self.children do
			self.children[i]:attach(tree, object)
		end
	end
end

function Node:get_state()
	return self.state
end

function Node:run(...)
	self.state = states.RUNNING

	self.tree:on_enter(self)

	if not self.started and self.on_start then
		self:on_start()
	end
	self.started = true

	local state = self:on_step(...)
	if state then
		assert(state == states.RUNNING or state == states.FAILED or state == states.SUCCESS)
		self.state = state
	else
		state = self:get_state()
	end

	self.tree:on_exit(self, state)

	if self.on_finish and (state == states.SUCCESS or state == states.FAILED) then
		self:on_finish(state)
	end

	return state
end

function behaviour.make_node(name, func, is_deco)
	local node = class(Node)
	function node:constructor(args, children, blackboard)
		self.args = args
		if is_deco then
			assert(#children == 1)
		end
		self.children = children
		self.blackboard = blackboard
	end

	node.on_step = func

	behaviour.register_factory(name, function(args, children, blackboard)
		return node:new(args, children, blackboard)
	end)

	return node
end

function behaviour.make_decorator(name, func)
	return behaviour.make_node(name, func, true)
end



BehaviourTree = class()
behaviour.BehaviourTree = BehaviourTree

function BehaviourTree:constructor(parent, node, object)
	assert(not parent or isDerivedFrom(parent, BehaviourTree))
	assert(isDerivedFrom(node, Node))
	assert(object and object.set_action)

	self.parent = parent
	self.children = { node }
	node:attach(self, object)
end

function BehaviourTree:on_enter(node)
	self.running_nodes[#self.running_nodes + 1] = node.typename

	if self.parent then
		self.parent:on_enter(node)
	end
end

function BehaviourTree:on_exit(node, state)
	if self.parent then
		self.parent:on_exit(node)
	end
end

function BehaviourTree:run(...)
	self.running_nodes = {}
	self.state = states.RUNNING

	local state = self.children[1]:run(...)
	if state then
		assert(state == states.RUNNING or state == states.FAILED or state == states.SUCCESS)
		self.state = state
	else
		state = self.state
	end

	return state
end



--
-- Behaviour Tree Primitives
--

-- A sequence will remember which was the last to run
local Sequence = class(Node)
behaviour.Sequence = Sequence
behaviour.register_factory("Sequence", function(args, children)
	assert(#children > 0)
	return Sequence:new(children)
end)

function Sequence:constructor(children)
	self.failed = false
	self.children = children
	self.index = 1
end

function Sequence:on_step(dtime)
	while self.index <= #self.children do
		local child = assert(self.children[self.index])

		local state = child:run(dtime)
		if state == states.RUNNING or state == states.FAILED then
			return state
		end

		self.index = self.index + 1
	end

	self.index = 1
	return states.SUCCESS
end


-- A selector will remember which was the last to run
local Selector = class(Node)
behaviour.Selector = Selector
behaviour.register_factory("Selector", function(args, children)
	assert(#children > 0)
	return Selector:new(children)
end)

behaviour.register_factory("fallback", function(args, children, blackboard)
	assert(#children == 2)
	return Selector:new(children)
end)

function Selector:constructor(children)
	self.failed = false
	self.children = children
	self.index = 1
end

function Selector:on_step(dtime)
	while self.index <= #self.children do
		local child = assert(self.children[self.index])

		local state = child:run(dtime)
		if state == states.RUNNING then
			return states.RUNNING
		elseif state == states.SUCCESS then
			self.index = 1
			return states.RUNNING
		end

		assert(state == states.FAILED)

		self.index = self.index + 1
	end
	return states.FAILED
end



-- A fickle sequence runs each child every tick
local FickleSequence = class(Node)
behaviour.FickleSequence = FickleSequence
behaviour.register_factory("FickleSequence", function(args, children)
	assert(#children > 0)
	return FickleSequence:new(children)
end)

function FickleSequence:constructor(children)
	self.children = children
	self.state = states.RUNNING
end

function FickleSequence:on_step(dtime)
	for i=1, #self.children do
		local child = self.children[i]
		local state = child:run(dtime)
		if state == states.RUNNING or state == states.FAILED then
			return state
		end

		assert(state == states.SUCCESS)
	end

	return states.SUCCESS
end


-- A fickle selector runs each child every tick
local FickleSelector = class(Node)
behaviour.FickleSelector = FickleSelector
behaviour.register_factory("FickleSelector", function(args, children)
	assert(#children > 0)
	return FickleSelector:new(children)
end)


function FickleSelector:constructor(children)
	self.children = children
	self.state = states.RUNNING
end

function FickleSelector:on_step(dtime)
	for i=1, #self.children do
		local child = self.children[i]
		local state = child:run(dtime)
		if state == states.RUNNING or state == states.SUCCESS then
			return state
		end

		assert(state == states.FAILED)
	end

	return states.FAILED
end
