local NPC = class(EntityController)
function NPC:on_create()
	self.entity:set_properties({ material = "player.png" })
	self.bt = behaviour.load_tree(self, "mods/core/behaviour/trees/npc.json")
end
function NPC:step(dtime)
	self.bt:run()

	if rvwp.debug then
		local nodename = "> " .. table.concat(self.bt.running_nodes, ", ")
		rvwp.debug:draw_label(self.entity:get_pos(), nodename)
	end
end

rvwp.entity.register("npc", NPC)


local Pedestrian = class(EntityController)
function Pedestrian:on_create()
	self.entity:set_properties({ material = "player.png" })
	self.bt = behaviour.load_tree(self, "mods/core/behaviour/trees/pedestrian.json")
end
function Pedestrian:step(dtime)
	self.bt:run()

	if rvwp.debug then
		rvwp.debug:draw_label(self.entity:get_pos(), "id: " .. self.entity:get_id())
	end
end

rvwp.entity.register("pedestrian", Pedestrian)


rvwp.register_chat_command("npc", commands.ChatCommand:new({
	func = function(self, name)
		local pos = rvwp.get_player_entity(name):get_pos()
		if rvwp.spawn_entity(pos, "npc") then
			return true, "Done"
		else
			return false, "Failed"
		end
	end
}))

rvwp.register_chat_command("ped", commands.ChatCommand:new({
	func = function(self, name)
		local pos = rvwp.get_player_entity(name):get_pos()
		if rvwp.spawn_entity(pos, "pedestrian") then
			return true, "Done"
		else
			return false, "Failed"
		end
	end
}))

rvwp.register_chat_command("stress", commands.ChatCommand:new({
	func = function(_, _, param)
		local count = tonumber(param)
		if not count then
			return false, "Usage: /stress 123"
		end

		for i=1, count do
			while true do
				local pos = V(math.random(-100, 100), math.random(-100, 100), 0)
				local surface = rvwp.get_surface_height(pos)
				if surface then
					pos.z = surface
					if rvwp.spawn_entity(pos, "pedestrian") then
						break
					end
				end
			end
		end
		return true, "Spawned " .. count .. " pedestrian NPCs"
	end
}))

