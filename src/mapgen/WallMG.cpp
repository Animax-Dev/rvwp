#include "../world/LightCalculator.hpp"
#include "WallMG.hpp"
#include "../content/content.hpp"

#include <random>

using namespace world;
using namespace MG;

void WallMG::generate(WorldChunk *c) {
	auto cpos = c->pos;
	auto c_wall = def->getCidFromName("wall");

	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<int> dist(1, 2);

	// Set terrain
	for (int i = 0; i < CHUNK_SIZE * CHUNK_SIZE; i++) {
		c->terrain[i] = (cpos.z == 0) ? def->getCidFromName("dirt")
									  : def->getCidFromName("void");

		c->lightLevels[i] = 0xF0;

		if (dist(gen) == 2) {
			c->tiles[i].cid = c_wall;
		}
	}

	LightCalculator light{def};
	light.initialSpread(c);
}
