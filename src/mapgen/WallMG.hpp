#pragma once

#include "Mapgen.hpp"

namespace MG {

class WallMG : public Mapgen {
public:
	explicit WallMG(content::DefinitionManager *def) : Mapgen(def) {}

	void generate(world::WorldChunk *c) override;
};

} // namespace MG
