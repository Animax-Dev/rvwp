#pragma once

#include "Inventory.hpp"
#include "InventoryCommand.hpp"

#include "events/EventBus.hpp"

#include <utility>

#include <functional>

namespace content {

/// Class to handle player selection logic, and generate InventoryCommands
class InventorySelection {
protected:
	StackLocation loc;
	int count = 0;
	InventoryProvider provider;

	EventBus<content::InventoryCommand> *inventoryBus;

	std::function<void(content::InventoryCommand)> onCommand;

public:
	explicit InventorySelection(InventoryProvider provider,
			EventBus<content::InventoryCommand> *inventoryBus = nullptr)
			: provider(std::move(provider)), inventoryBus(inventoryBus) {}

	InventorySelection(const InventorySelection &other) = delete;

	bool empty() { return count == 0; }

	void select(const StackLocation &location, int count);

	void deselect() {
		loc = {};
		count = 0;
	}

	const StackLocation &getSelection() const { return loc; }

	int getCount() const { return count; }

	/// Move or swap the selection to/with the given slot
	bool moveTo(const StackLocation &location2, int count2);
};

} // namespace content
