#include "content.hpp"
#include "RegistrationHelpers.hpp"
#include "DefinitionManager.hpp"

#include <log.hpp>
#include <libs/sanity.hpp>

using namespace content;

void DefinitionManager::defineVoid() {
	registerTile(simpleTile("void", "Void", Material(0, 2), true));
	SanityCheck(getCidFromName("void") == CID_VOID);
}

ItemDef *DefinitionManager::registerItem(ItemDef *def) {
	SanityCheck(!locked);
	assert(!def->name.empty());
	SanityCheck(items_by_name.find(def->name) == items_by_name.end())

			items.push_back(std::unique_ptr<ItemDef>(def));
	items_by_name[def->name] = def;

	return def;
}

TileDef *DefinitionManager::registerTile(TileDef *def) {
	if (registerItem(def) == nullptr) {
		return nullptr;
	}

	if (def->id == 0) {
		contentIdCounter++;
		def->id = contentIdCounter;
	} else {
		SanityCheck(tiles_by_cid.find(def->id) ==
				tiles_by_cid.end()) if (def->id > contentIdCounter) {
			contentIdCounter = def->id;
		}
	}

	tiles_by_cid[def->id] = def;

	return def;
}

ItemDef *DefinitionManager::getItemDef(const std::string &name) {
	auto it = items_by_name.find(name);
	if (it == items_by_name.end()) {
		return nullptr;
	} else {
		return it->second;
	}
}

TileDef *DefinitionManager::getTileDef(content_id id) {
	if (id < 1 || id > contentIdCounter) {
		Log("Content", WARNING) << "Tiledef not found! " << (int)id;
		return nullptr;
	}

	return tiles_by_cid[id];
}

TileDef *DefinitionManager::getTileDef(const std::string &name) {
	ItemDef *def = getItemDef(name);
	if (def && def->getType() > ItemType::Item) {
		return (TileDef *)(def);
	} else {
		return nullptr;
	}
}

content_id DefinitionManager::getCidFromName(const std::string &name) {
	TileDef *def = getTileDef(name);

	return def ? def->id : 0;
}
