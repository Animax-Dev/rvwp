#include "KeyValueStore.hpp"

#include <algorithm>
#include <cctype>

using namespace content;

template <>
std::optional<std::string> KeyValueStore::get(const std::string &key) const {
	auto it = values.find(key);
	if (it == values.end()) {
		return {};
	}

	return {it->second};
}

template <>
std::optional<int> KeyValueStore::get(const std::string &key) const {
	auto it = values.find(key);
	if (it == values.end()) {
		return {};
	}

	return {std::stoi(it->second)};
}

template <>
std::optional<float> KeyValueStore::get(const std::string &key) const {
	auto it = values.find(key);
	if (it == values.end()) {
		return {};
	}

	return {std::stof(it->second)};
}

template <>
std::optional<double> KeyValueStore::get(const std::string &key) const {
	auto it = values.find(key);
	if (it == values.end()) {
		return {};
	}

	return {std::stod(it->second)};
}

template <>
std::optional<bool> KeyValueStore::get(const std::string &key) const {
	auto optionalValue = get<std::string>(key);
	if (!optionalValue.has_value()) {
		return {};
	}

	std::string value = optionalValue.value();
	std::transform(value.begin(), value.end(), value.begin(),
			[](unsigned char c) { return std::tolower(c); });

	return value == "yes" || value == "true" || value == "on" || value == "1";
}
