#pragma once

#include "content.hpp"

namespace content {

class ItemStack {
public:
	std::string name;
	int count;

	int quality = 100;
	int health = 0;

	// TODO: Metadata?

	ItemStack(const std::string &name, int count) : name(name), count(count) {}

	ItemStack() : ItemStack("", 0) {}

	inline bool empty() const { return count == 0 || name.empty(); }

	bool operator==(const ItemStack &other) const {
		return (empty() && other.empty()) ||
				(name == other.name && count == other.count &&
						quality == other.quality && health == other.health);
	}

	inline bool operator!=(const ItemStack &other) const {
		return !(*this == other);
	}
};

} // namespace content
