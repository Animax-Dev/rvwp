#pragma once

#include <optional>

#include "world/Chunk.hpp"
#include "Material.hpp"

namespace content {

enum class ToolType { CANCEL, BUILD, DECONSTRUCT };

enum class ToolSelectType {
	FILLED, ///< Filled selection area
	BORDER, ///< Border of selection only
	FIXED	///< Fixed size selection
};

static inline std::optional<ToolType> strToToolType(const std::string &str) {
	if (str == "cancel") {
		return ToolType::CANCEL;
	} else if (str == "build") {
		return ToolType::BUILD;
	} else if (str == "deconstruct") {
		return ToolType::DECONSTRUCT;
	} else {
		return {};
	}
}

static inline std::optional<ToolSelectType> strToToolSelectType(
		const std::string &str) {
	if (str == "filled") {
		return ToolSelectType::FILLED;
	} else if (str == "border") {
		return ToolSelectType::BORDER;
	} else if (str == "fixed") {
		return ToolSelectType::FIXED;
	} else {
		return {};
	}
}

struct ToolSpec {
	ToolType type;
	std::string description;
	content::Material material;
	ToolSelectType selectType;
	V3s fixedSelectionSize;

	std::string itemName;
	world::ChunkLayer layer;
};

} // namespace content
