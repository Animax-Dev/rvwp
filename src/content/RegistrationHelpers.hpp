#pragma once

#include "ItemDef.hpp"

namespace content {
inline TileDef *simpleTile(std::string name, std::string title,
		Material material, bool collides,
		const std::string &footstepClass = "") {
	auto def = new TileDef();
	def->name = std::move(name);
	def->title = std::move(title);
	def->material = std::move(material);
	def->collides = collides;
	def->footstepSound = footstepClass + "_footstep";
	return def;
}

inline TileDef *floorTile(std::string name, std::string title,
		Material material, const std::string &footstepClass) {
	auto def = new TileDef(ItemType::Floor);
	def->name = std::move(name);
	def->title = std::move(title);
	def->material = std::move(material);
	def->collides = false;
	def->footstepSound = footstepClass + "_footstep";
	return def;
}

inline TileDef *lightTile(std::string name, std::string title,
		Material material, u8 lightSource) {
	auto def = simpleTile(
			std::move(name), std::move(title), std::move(material), false);
	def->lightSource = lightSource;
	def->lightPropagates = true;
	return def;
}

inline TileDef *buildingTile(
		std::string name, std::string title, Material material, bool collides) {
	auto def = new TileDef();
	def->name = std::move(name);
	def->title = std::move(title);
	def->material = std::move(material);
	def->collides = collides;
	def->lightPropagates = true;
	return def;
}

inline ItemDef *simpleItem(
		std::string name, std::string title, Material material) {
	auto def = new ItemDef();
	def->name = std::move(name);
	def->title = std::move(title);
	def->material = std::move(material);
	return def;
}

inline ItemDef *weapon(std::string name, std::string title, Material material,
		const WeaponSpec &weaponSpec) {
	auto def =
			simpleItem(std::move(name), std::move(title), std::move(material));
	def->weapon_spec = weaponSpec;
	return def;
}

inline ToolSpec makeBuildSpec(TileDef *tileDef) {
	ToolSelectType selectType = ToolSelectType::BORDER;
	V3s fixedSize;
	switch (tileDef->material.type) {
	case content::EMT_CON_8BIT:
		selectType = ToolSelectType::BORDER;
		break;
	case content::EMT_STATIC:
	case content::EMT_DOORLIKE:
		selectType = ToolSelectType::FIXED;
		fixedSize = {1, 1, 1};
		break;
	}

	return {ToolType::BUILD, tileDef->title, tileDef->material, selectType,
			fixedSize, tileDef->name, world::ChunkLayer::Tile};
}

} // namespace content
