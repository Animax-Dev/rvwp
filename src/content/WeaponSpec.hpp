#pragma once

#include "../types/types.hpp"

#include <array>

namespace content {

class WeaponSpec {
public:
	enum class Type { None = 0, Melee, Gun };

	Type type = Type::None;

	/// Maximum range before the chance of hitting becomes 0
	f32 range = 10;

	/// Damage on successful direct hit (inverse square otherwise)
	u16 damage = 10;

	/// How long it takes to reload. This will be low for machine guns and high
	/// for shotguns
	f32 useInterval = 0.5;

	static WeaponSpec makeStandardGun(
			float range, u16 damage, float fire_interval) {
		return {Type::Gun, range, damage, fire_interval};
	}
};

static inline WeaponSpec::Type stringToWeaponType(const std::string &str) {
	if (str == "melee") {
		return WeaponSpec::Type::Melee;
	} else if (str == "gun") {
		return WeaponSpec::Type::Gun;
	} else {
		return WeaponSpec::Type::None;
	}
}

} // namespace content
