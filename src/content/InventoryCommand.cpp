#include "InventoryCommand.hpp"

using namespace content;

namespace {

void setStack(const InventoryProvider &provider, const StackLocation &location,
		ItemStack stack) {
	auto list = provider.getList(location.inv, location.list).value_or(nullptr);
	assert(list);

	list->set(location.idx, stack);
}

} // namespace

bool InventoryCommand::perform(const InventoryProvider &provider) {
	auto srcStack = provider.getStack(source);
	auto dstStack = provider.getStack(dest);

	switch (type) {
	case SWAP:
		setStack(provider, source, dstStack);
		setStack(provider, dest, srcStack);
		return true;
	case MOVE:
		if (!dstStack.empty() && dstStack.name != srcStack.name) {
			return false;
		}

		if (dstStack.empty()) {
			dstStack = srcStack;
			dstStack.count = 0;
		}

		srcStack.count -= count;
		dstStack.count += count;
		setStack(provider, source, srcStack);
		setStack(provider, dest, dstStack);
		return true;
	}

	assert(false);
	return false;
}
