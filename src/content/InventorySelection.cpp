#include "InventorySelection.hpp"

using namespace content;

void InventorySelection::select(const StackLocation &location, int _count) {
	loc = location;
	count = _count;
}

bool InventorySelection::moveTo(const StackLocation &location2, int count2) {
	if (loc == location2) {
		deselect();
		return false;
	}

	auto source = provider.getStack(loc);
	auto dest = provider.getStack(location2);
	auto type = (source.count == count && source.name != dest.name)
			? InventoryCommand::SWAP
			: InventoryCommand::MOVE;

	auto cmd = InventoryCommand(type, loc, location2, count);
	if (!cmd.perform(provider)) {
		return false;
	}

	if (inventoryBus) {
		inventoryBus->push(cmd);
	}

	switch (cmd.getType()) {
	case InventoryCommand::SWAP:
		select(loc, provider.getStack(loc).count);
		break;
	case InventoryCommand::MOVE:
		deselect();
		break;
	}

	return true;
}
