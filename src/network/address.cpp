#include "address.hpp"
#include <enet/enet.h>
#include <sstream>

using namespace network;

Address::Address(const std::string &host, unsigned short port) : port(port) {
	ENetAddress eaddress;
	enet_address_set_host(&eaddress, host.c_str());
	address = eaddress.host;
}

std::string Address::getAddressString() const {
	std::ostringstream os;
	os << (int)GetA();
	os << '.';
	os << (int)GetB();
	os << '.';
	os << (int)GetC();
	os << '.';
	os << (int)GetD();

	return os.str();
}
