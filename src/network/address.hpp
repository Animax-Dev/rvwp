#pragma once
#include "../types/types.hpp"

namespace network {

class Address {
	u32 address;
	unsigned short port;

public:
	Address() {
		address = 0;
		port = 0;
	}

	Address(unsigned char a, unsigned char b, unsigned char c, unsigned char d,
			unsigned short port) {
		this->address = (d << 24) | (c << 16) | (b << 8) | a;
		this->port = port;
	}

	Address(unsigned int address, unsigned short port) {
		this->address = address;
		this->port = port;
	}

	Address(const std::string &address, unsigned short port);

	unsigned int GetAddress() const { return address; }

	unsigned char GetA() const { return (unsigned char)(address); }

	unsigned char GetB() const { return (unsigned char)(address >> 8); }

	unsigned char GetC() const { return (unsigned char)(address >> 16); }

	unsigned char GetD() const { return (unsigned char)(address >> 24); }

	unsigned short GetPort() const { return port; }

	std::string getAddressString() const;

	bool operator==(const Address &other) const {
		return address == other.address && port == other.port;
	}

	bool operator>(const Address &other) const {
		return address > other.address || port > other.port;
	}

	bool operator<(const Address &other) const {
		return address < other.address || port < other.port;
	}

	bool operator!=(const Address &other) const { return !(*this == other); }
};
} // namespace network
