#pragma once

#include <SFML/Graphics.hpp>
#include <string>

namespace sfext {
class DropShadow : public sf::Drawable {
	const sf::Drawable &drawable;
	const sf::Vector2f size;
	const sf::Vector2f offset;
	const sf::Color color;

	sf::RenderTexture texture;
	sf::Vector2f pixelSize;

public:
	DropShadow(const sf::Drawable &drawable, sf::Vector2f size,
			sf::Vector2f offset, sf::Color color)
			: drawable(drawable), size(size), offset(offset), color(color) {
		regenerate();
	}

	static void setShader(sf::Shader *v);

private:
	virtual void draw(
			sf::RenderTarget &target, sf::RenderStates states) const override;

	void regenerate();
};
} // namespace sfext
