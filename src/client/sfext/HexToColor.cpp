#include "HexToColor.hpp"
#include "../../types/types.hpp"
#include <sstream>

bool sfe::HexToColor(const std::string &hex, sf::Color &color) {
	size_t length = hex.size();

	std::istringstream hexss(hex);
	if (hexss.peek() == '#') {
		hexss.ignore(1);
		length--;
	}

	bool longForm = length >= 6;
	if ((longForm && length != 6 && length != 8) ||
			(!longForm && length != 3 && length != 4)) {
		return false;
	}
	bool hasAlpha = longForm ? length == 8 : length == 4;

	u8 colorComps[] = {0, 0, 0, 255};
	for (int i = 0; i < (hasAlpha ? 4 : 3); i++) {
		char comp[3];
		hexss.read(&comp[0], 2);
		comp[2] = '\0';
		colorComps[i] = strtoul(comp, nullptr, 16);
	}

	color = sf::Color(
			colorComps[0], colorComps[1], colorComps[2], colorComps[3]);

	return true;
}
