#pragma once
#include <SFML/Graphics.hpp>

namespace sfe {

bool HexToColor(const std::string &hex, sf::Color &color);

}
