#include "ChatView.hpp"
#include "HexToColor.hpp"
#include <cassert>

using namespace sfext;

ChatView::ChatView(
		const sf::Font &font, unsigned int characterSize, int numberLines)
		: font(font), chat_text(), numberLines(numberLines) {}

void ChatView::draw(sf::RenderTarget &target, sf::RenderStates states) const {
	auto pos = getPosition();
	const int line_height = 20;
	pos.y += line_height * chat_text.size();

	for (auto label : chat_text) {
		label.setPosition(pos);
		target.draw(label, states);
		pos.y -= line_height;
	}
}

void ChatView::pushChatMessage(const std::string &msg) {
	if (chat_text.size() >= numberLines) {
		chat_text.pop_back();
	}

	sfe::RichText ele(font);

	sf::String str = sf::String::fromUtf8(msg.begin(), msg.end());

	// The first index of characters that haven't been flushed yet
	size_t unprocessedStart = 0;

	// Iterate through string
	for (size_t i = 0; i < str.getSize(); i++) {
		if (str[i] != 0x1B) {
			continue;
		}

		ele << str.substring(unprocessedStart, i - unprocessedStart);
		unprocessedStart = i;

		i++;
		assert(i < str.getSize());
		if (i >= str.getSize()) {
			break;
		}

		// First character is a bracket
		assert(str[i] == '(');
		if (str[i] != '(') {
			continue;
		}

		// Get the command
		i++;
		sf::Uint32 cmdStart = i;
		while (i < str.getSize() && str[i] != '@') {
			i++;
		}
		assert(i < str.getSize());
		if (i >= str.getSize()) {
			break;
		}
		assert(str[i] == '@');
		sf::String cmd = str.substring(cmdStart, i - cmdStart);

		// Get the value
		i++;
		sf::Uint32 valueStart = i;
		while (i < str.getSize() && str[i] != ')') {
			i++;
		}
		assert(i < str.getSize());
		if (i >= str.getSize()) {
			break;
		}
		sf::String value = str.substring(valueStart, i - valueStart);
		assert(str[i] == ')');
		i++;

		// Do command
		if (cmd == "c") {
			sf::Color color;
			if (sfe::HexToColor(value, color)) {
				ele << color;
			} else {
				LogF(ERROR, "[HudView] Unrecognised color (%s @ %s)",
						cmd.toAnsiString().c_str(),
						value.toAnsiString().c_str());
			}
		} else {
			LogF(ERROR, "[HudView] Unrecognised escape sequence! (%s @ %s)",
					cmd.toAnsiString().c_str(), value.toAnsiString().c_str());
		}

		unprocessedStart = i;
	}

	// Flush remaining
	if (unprocessedStart != str.getSize()) {
		ele << str.substring(
				unprocessedStart, str.getSize() - unprocessedStart);
	}

	ele.setCharacterSize(18);
	ele.setOrigin(0, ele.getGlobalBounds().height);
	chat_text.push_front(ele);
}

sf::Vector2f ChatView::getSize() {
	float width = 0;
	float height = 0;

	for (const auto &label : chat_text) {
		width = std::max(width, label.getGlobalBounds().width);
		height += label.getGlobalBounds().height;
	}

	return {width, height};
}
