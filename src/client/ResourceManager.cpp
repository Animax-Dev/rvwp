#include <sanity.hpp>
#include <random>

#include "ResourceManager.hpp"

using namespace client;

sf::Sprite ClientMaterial::toSprite() {
	sf::Sprite sprite;
	if (texture) {
		sprite.setTexture(*texture);
	}
	sprite.setTextureRect(sf::IntRect(
			getTopRightPosInTexture().x, getTopRightPosInTexture().y, w, h));
	return sprite;
}

void ClientMaterial::draw(sf::RenderTarget &target, sf::Vector2f pos) {
	sf::Sprite sprite = toSprite();
	sprite.setPosition(pos);
	target.draw(sprite);
}

sf::Texture *ResourceManager::getTexture(const std::string &name) {
	auto item = textures_lookup.find(name);
	if (item == textures_lookup.end()) {
		LogF(WARNING, "[ResourceManager] Unable to find texture %s",
				name.c_str());
		return nullptr;
	} else {
		return item->second;
	}
}

sf::Texture *ResourceManager::getTextureOrLoad(const std::string &name) {
	auto it = textures_lookup.find(name);
	if (it != textures_lookup.end()) {
		return it->second;
	}

	auto newPtr = std::make_unique<sf::Texture>();
	sf::Texture *txt = newPtr.get();

	if (!newPtr->loadFromFile("assets/textures/" + name)) {
		LogF(ERROR, "[ResourceManager] Failed to load assets/textures/%s",
				name.c_str());
		return nullptr;
	}

	LogF(INFO, "[ResourceManager] Loaded assets/textures/%s", name.c_str());

	textures_lookup[name] = txt;
	textures.push_back(std::move(newPtr));
	return txt;
}

ClientMaterial ResourceManager::getMaterial(content::Material mat) {
	ClientMaterial cmat(mat);
	cmat.texture = getTexture(mat.name);
	cmat.startx = 0;
	cmat.starty = 0;

	// TODO: texture atlas

	return cmat;
}

ClientMaterial ResourceManager::getMaterialOrLoad(content::Material mat) {
	ClientMaterial cmat(mat);
	cmat.texture = getTextureOrLoad(mat.name);
	cmat.startx = 0;
	cmat.starty = 0;

	// TODO: texture atlas

	return cmat;
}

sf::Texture *ResourceManager::getTileset(TextureType type) {
	switch (type) {
	case TextureType::DIFFUSE:
		return getTextureOrLoad("tileset.diffuse.png");
	case TextureType::NORMAL:
		return getTextureOrLoad("tileset.normal.png");
	case TextureType::BLUEPRINT:
		return getTextureOrLoad("tileset.blueprint.png");
	}

	FatalError("Unsupported texture type");
}

SoundSet::SoundSet(const std::string &name) {
	for (int i = 1;; i++) {
		auto newPtr = std::make_unique<sf::SoundBuffer>();
		if (!newPtr->loadFromFile("assets/sounds/" + name + "." +
					std::to_string(i) + ".ogg")) {
			break;
		}

		sounds.push_back(std::move(newPtr));
	}

	if (sounds.empty()) {
		LogF(WARNING, "[ResourceManager] Unable to load sound %s",
				name.c_str());
	}
}

const sf::SoundBuffer &SoundSet::getRandom() const {
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> distrib(0, sounds.size() - 1);
	return *sounds[distrib(gen)];
}

const SoundSet *ResourceManager::getSound(const std::string &name) {
	auto item = sounds_lookup.find(name);
	if (item == sounds_lookup.end()) {
		LogF(WARNING, "[ResourceManager] Unable to find sound %s",
				name.c_str());
		return nullptr;
	} else {
		return &item->second;
	}
}

const SoundSet *ResourceManager::getSoundOrLoad(const std::string &name) {
	auto it = sounds_lookup.find(name);
	if (it != sounds_lookup.end()) {
		return &it->second;
	}

	SoundSet sounds(name);
	if (!sounds.valid()) {
		return nullptr;
	}

	auto it2 = sounds_lookup.insert(std::make_pair(name, std::move(sounds)));
	return &it2.first->second;
}
