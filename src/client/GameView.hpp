#pragma once

#include <memory>

#include "events/EventListener.hpp"
#include "content/InventoryCommand.hpp"

#include "../world/WorldEvent.hpp"
#include "../events/EventBus.hpp"

#include "models/HeroEvent.hpp"

#include "views/world/WorldView.hpp"
#include "views/HudView.hpp"
#include "views/StackView.hpp"
#include "views/ChatSendView.hpp"
#include "views/HudView.hpp"
#include "client/views/hero/HeroView.hpp"
#include "views/architect/ArchitectView.hpp"

namespace client {

class Game;

/// Super View to contain and manage all Views
class HeroController;
class GameView : public View {
	WorldView::Ptr worldView;
	HudView::Ptr hudView;

	StackView::Ptr invView;
	StackView::Ptr chatSendView;
	StackView::Ptr heroView;
	StackView::Ptr archView;

	EventListener<HeroEvent> heroListener;

	View::Ptr mainView;
	View::Ptr rootView;

public:
	GameView(Game *game, EventBus<world::WorldEvent> *worldBus,
			EventBus<HeroEvent> *heroEvent,
			EventBus<content::InventoryCommand> *inventoryBus,
			content::DefinitionManager *def, ResourceManager *resources,
			tgui::Gui &gui);

	/// Runs deactivate and activate
	/// @return true on success, false on failure
	bool setRootView(View::Ptr newctr);

	void switchToHeroMode() { setMainView(heroView); }

	void switchToArchitectMode() { setMainView(archView); }

	/// Delegates to root view controller
	void draw(sf::RenderTarget &target, sf::RenderStates states) override;

	/// Delegates to root view controller
	void drawHUD(sf::RenderTarget &target, sf::RenderStates states) override;

	/// Delegates to root view controller
	void drawPostGUI(
			sf::RenderTarget &target, sf::RenderStates states) override;

	/// Delegates to root view controller
	void update(float dtime) override;

	/// Load resources
	bool load() override;

	/// TODO: remove this
	/// @return HUD view controller
	HudView *getHUDView() { return hudView.get(); }

	/// TODO: remove this
	/// @return world view controller
	WorldView *getWorldView() { return worldView.get(); }

	void resize(unsigned int width, unsigned int height) override;

	/// Whether main / hud view is active
	bool isMainViewActive() const;

	/// Switch to main / hud view
	void switchToMainView() { setRootView(mainView); }

	/// Switch to inventory view
	void switchToInvView() { setRootView(invView); }

	/// Switch to send chat view
	///
	/// @param msg Prefilled chat message
	void switchToChatSendView(const std::string &msg = "");

	/// Function to map a screen-space co-ordinate to the world
	///
	/// @return The function
	std::function<V3f(V2s)> getScreenToWorldMapper(
			sf::RenderWindow *window) const;
	bool exit() override;

protected:
	/// Sets the new main view
	void setMainView(View::Ptr ctr) {
		bool needsChange = isMainViewActive();
		mainView = std::move(ctr);
		if (needsChange) {
			setRootView(mainView);
		}
	}

	void handleHeroEvent(const HeroEvent &event);
};

} // namespace client
