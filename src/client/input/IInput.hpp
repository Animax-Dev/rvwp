#pragma once

#include <SFML/System.hpp>
#include <SFML/Window/Event.hpp>
#include <optional>
#include "types/types.hpp"
#include "Action.hpp"
#include "InputConnection.hpp"

namespace client {

class IInput {
public:
	virtual ~IInput() = default;

	/// Return the action state
	///
	/// @return action state
	virtual ActionState getCurrentActionState() const = 0;

	/// Set the ActionState provider
	///
	/// @param v ActionState provider
	virtual void setActionStateProvider(
			const std::function<ActionState()> &v) = 0;

	/// Get movement vector.
	///
	/// In hero mode, this is the player movement.
	/// In architect mode, this is the camera movement.
	///
	/// @return normalised vector
	virtual V3f getMoveVector() = 0;

	/// Return the cursor's current position. For keyboard+mouse,
	/// this is just the mouse position. For gamepad, this is relative
	/// to the player position - called "origin" for abstraction here.
	///
	/// This differs from a selection in that raycasting isn't done,
	/// and it is an exact floating-point position rather than integer.
	///
	/// @param origin Usually the player position
	/// @return Optional cursor position, in world co-ordinates
	virtual std::optional<V3f> getCursorPosition(const V3f &origin) = 0;

	/// @return Whether the cursor is virtual, ie: a gamepad joystick
	virtual bool isVirtualCursor() const = 0;

	/// Subscribe to an action
	///
	/// @param action The action
	/// @param listener Listener function
	/// @returns InputConnection Store this to avoid the listener from being
	/// de-registered.
	[[nodiscard]] virtual InputConnection connect(
			Action action, const std::function<void()> &listener) = 0;

	/// Whether an action is active
	///
	/// @param action Action
	/// @return Is pressed
	virtual bool isActive(Action action) = 0;
};

class IInputController : public Controller, public IInput {
public:
	virtual bool onEvent(const sf::Event &event) = 0;
};

} // namespace client
