#include "InputController.hpp"

using namespace client;

V3f InputController::getMoveVector() {
	return firstInResolvers<V3f>([](auto resolver) -> std::optional<V3f> {
		auto move_vector = resolver->getMoveVector();
		if (move_vector.sqLength() > 0) {
			move_vector.normalise2();
			return move_vector;
		}

		return std::nullopt;
	}).value_or(V3f{});
}

std::optional<V3f> InputController::getCursorPosition(const V3f &origin) {
	return firstInResolvers<V3f>([&](auto resolver) -> std::optional<V3f> {
		return resolver->getCursorPosition(origin);
	});
}

bool InputController::isVirtualCursor() const {
	const IInputResolver *recent = getMostRecentlyActiveResolver();
	return recent && recent->isVirtualCursor();
}

bool InputController::isActive(Action action) {
	return firstInResolvers<bool>([&](auto resolver) -> std::optional<bool> {
		return resolver->getActionMap().isActive(action);
	}).value_or(false);
}

bool InputController::onEvent(const sf::Event &event) {
	for (const auto &resolver : input_resolvers) {
		auto &map = resolver->getActionMap();
		map.pushEvent(event);
		map.invokeCallbacks(system, nullptr);
		map.clearEvents();
	}

	return false;
}

void InputController::update(float dtime) {
	for (const auto &resolver : input_resolvers) {
		resolver->update(dtime);
	}

	inputListeners.erase(
			std::remove_if(inputListeners.begin(), inputListeners.end(),
					[](auto &x) { return !x->valid(); }),
			inputListeners.end());
}

IInputResolver *InputController::getMostRecentlyActiveResolver() const {
	IInputResolver *result = nullptr;

	float bestTime = 0.f;

	for (const auto &resolver : input_resolvers) {
		auto value = resolver->getTimeSinceLastActive();
		if (result == nullptr || value < bestTime) {
			bestTime = value;
			result = resolver.get();
		}
	}

	return result;
}

template <typename T>
std::optional<T> InputController::firstInResolvers(
		std::function<std::optional<T>(IInputResolver *)> func) {
	// Search most recently active
	IInputResolver *recent = getMostRecentlyActiveResolver();
	if (recent) {
		auto ret = func(recent);
		if (ret.has_value()) {
			return ret.value();
		}
	}

	// Then search remaining
	for (const auto &resolver : input_resolvers) {
		if (resolver.get() != recent) {
			auto ret = func(resolver.get());
			if (ret.has_value()) {
				return ret.value();
			}
		}
	}

	return {};
}
