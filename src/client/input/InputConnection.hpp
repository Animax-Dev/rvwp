#pragma once

#include <Thor/Input.hpp>
#include <log.hpp>
#include <utility>
#include "Action.hpp"

namespace client {

class InputListener {
	std::function<void(thor::ActionContext<Action>)> func;
	thor::Connection connection;

public:
	InputListener(std::function<void(thor::ActionContext<Action>)> func,
			thor::ActionMap<Action>::CallbackSystem &system, Action action)
			: func(std::move(func)) {
		connection =
				system.connect(action, [&](auto context) { (*this)(context); });
	}

	InputListener(const InputListener &other) = delete;

	InputListener(InputListener &&other) noexcept {
		connection = other.connection;
		other.connection.invalidate();
	}

	~InputListener() { connection.disconnect(); }

	void operator()(thor::ActionContext<Action> context) {
		if (func) {
			func(context);
		}
	}

	bool valid() const { return (bool)func; }

	void disconnect() { func = {}; }
};

class InputConnection {
	InputListener *listener = nullptr;

public:
	InputConnection() = default;

	explicit InputConnection(InputListener *listener) : listener(listener) {}

	InputConnection(const InputConnection &other) = delete;

	InputConnection(InputConnection &&other) noexcept {
		listener = other.listener;
		other.listener = nullptr;
	}

	~InputConnection() {
		if (listener) {
			listener->disconnect();
		}
	}
};

} // namespace client
