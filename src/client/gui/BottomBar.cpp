#include "BottomBar.hpp"

using namespace client;

BottomBar::BottomBar() : Panel("BottomBar") {
	setRenderer(tgui::Theme::getDefault()->getRendererNoThrow(m_type));

	recreate();
}

BottomBar::Ptr BottomBar::create(const tgui::Layout2d &size) {
	const auto panel = std::make_shared<BottomBar>();
	panel->setSize(size);
	return panel;
}

void BottomBar::addButton(
		const std::string &text, sf::Texture *texture, tgui::Widget::Ptr view) {
	items.emplace_back(text, tgui::Texture(*texture), view);
	recreate();
}

void BottomBar::openTab(int idx) {
	if (selectedIdx >= 0) {
		closeTab();
	}

	if (idx < items.size()) {
		const auto &item = items[idx];
		selectedIdx = idx;

		item.view->setVisible(true);
		item.view->setFocused(true);
	}
}

void BottomBar::toggleTab(int idx) {
	if (selectedIdx == idx) {
		closeTab();
	} else {
		openTab(idx);
	}
}

bool BottomBar::closeTab() {
	if (selectedIdx >= items.size() || selectedIdx < 0) {
		selectedIdx = -1;
		return false;
	}

	const auto &item = items[selectedIdx];
	if (item.view->isFocused()) {
		item.button->setFocused(true);
	}

	item.view->setVisible(false);
	selectedIdx = -1;
	return true;
}

void BottomBar::recreate() {
	if (!layout) {
		layout = tgui::HorizontalLayout::create({"100%", "100%"});
		add(layout);
	}

	layout->removeAllWidgets();

	auto buttonTheme =
			tgui::Theme::getDefault()->getRendererNoThrow("BottomBarButton");

	size_t middleAt = items.size() / 2;

	for (size_t i = 0; i < items.size(); i++) {
		if (i == middleAt) {
			auto label = tgui::Label::create("RVWP");
			label->setHorizontalAlignment(
					tgui::Label::HorizontalAlignment::Center);
			label->setVerticalAlignment(tgui::Label::VerticalAlignment::Center);
			label->ignoreMouseEvents(true);
			layout->add(label, 3);
		}

		auto &item = items[i];

		item.view->setVisible(selectedIdx == i);
		item.view->setPosition({"0", "BottomBar.y - height"});

		auto button = tgui::BitmapButton::create("   " + item.text);
		button->setRenderer(buttonTheme);
		button->onPress(&BottomBar::toggleTab, this, i);
		if (item.texture) {
			button->setImage(item.texture.value());
			button->setImageScaling(32.f / 40.f);
		}

		std::string buttonName = "BottomBtn" + std::to_string(i);
		layout->add(button, buttonName);

		item.button = button;
	}
}
