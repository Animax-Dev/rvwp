#include "ToolPanel.hpp"

using namespace client;
using namespace content;

ToolPanel::ToolPanel(ResourceManager *resourceManager)
		: tgui::ScrollablePanel("ToolPanel"), resourceManager(resourceManager) {

	onFocus.connect([&]() {
		if (m_focusedWidget) {
			// There was already a focused widget, force a redraw
			auto leaf = getFocusedLeaf();
			leaf->setFocused(false);
			leaf->setFocused(true);
		} else if (firstButton) {
			firstButton->setFocused(true);
		}
	});

	setRenderer(tgui::Theme::getDefault()->getRenderer("Panel"));
	setVerticalScrollbarPolicy(tgui::Scrollbar::Policy::Never);
	setHorizontalScrollbarPolicy(tgui::Scrollbar::Policy::Always);
	setHorizontalScrollAmount(20);
}

ToolPanel::Ptr ToolPanel::create(
		ResourceManager *resourceManager, const tgui::Layout2d &size) {
	const auto panel = std::make_shared<ToolPanel>(resourceManager);
	panel->setSize(size);
	return panel;
}

void ToolPanel::setSpecs(const std::vector<ToolSpec> &v) {
	specs = v;
	recreate();
}

void ToolPanel::recreate() {
	firstButton = {};
	shortcutKeyToButtons.clear();
	removeAllWidgets();

	auto grid = tgui::Grid::create();
	grid->setPosition({"0", "0"});
	grid->setAutoSize(true);
	add(grid);

	for (size_t i = 0; i < specs.size(); i++) {
		const auto &spec = specs[i];

		auto button = tgui::BoxLayout::create({"74", "90"});

		auto cMaterial = resourceManager->getMaterialOrLoad(spec.material);
		if (cMaterial.texture) {
			auto image = tgui::Picture::create(tgui::Texture(*cMaterial.texture,
					tgui::UIntRect(cMaterial.getURectInTexture())));
			image->setPosition("5", "5");
			image->setSize("64", "64");
			image->ignoreMouseEvents(true);
			button->add(image);
		}

		tgui::String sfDescription = spec.description;
		auto label = tgui::Label::create(sfDescription);
		label->setHorizontalAlignment(tgui::Label::HorizontalAlignment::Center);
		label->setVerticalAlignment(tgui::Label::VerticalAlignment::Bottom);
		label->setPosition("5", "0");
		label->setSize("100% - 10", "100% - 5");
		label->ignoreMouseEvents(true);
		button->add(label);

		auto actualButton = tgui::Button::create("");
		actualButton->setRenderer(
				tgui::Theme::getDefault()->getRenderer("FlatButton"));
		actualButton->setPosition("0", "0");
		actualButton->setSize({"100%", "100%"});
		actualButton->onPress(onToolSelected, spec);
		button->add(actualButton);

		if (!firstButton) {
			firstButton = actualButton;
		}

		auto &list = shortcutKeyToButtons[std::tolower(sfDescription[0])];
		list.push_back(actualButton);

		grid->addWidget(button, i % 2, i / 2);
	}
}

void ToolPanel::textEntered(char32_t key) {
	Container::textEntered(key);

	auto it = shortcutKeyToButtons.find(key);
	if (it == shortcutKeyToButtons.end()) {
		return;
	}

	bool setNext = false;
	for (const auto &ele : it->second) {
		if (ele->isFocused()) {
			setNext = true;
		} else if (setNext) {
			ele->setFocused(true);
			ele->onPress.emit(ele.get(), "");
			return;
		}
	}

	{
		auto &ele = it->second[0];
		ele->setFocused(true);
		ele->onPress.emit(ele.get(), "");
	}
}
