#pragma once

#include <TGUI/TGUI.hpp>
#include "content/Inventory.hpp"
#include "../ResourceManager.hpp"
#include "InvSelection.hpp"

namespace client {

class InvListWidget : public tgui::Grid {
	content::InventoryLocation inventoryLocation;
	std::string listName;
	content::InventoryProvider provider;

	content::DefinitionManager *defMan;
	ResourceManager *resourceManager;
	InvSelection &selection;

public:
	InvListWidget(InvSelection &selection, content::DefinitionManager *defMan,
			ResourceManager *resourceManager,
			content::InventoryProvider provider)
			: selection(selection), defMan(defMan),
			  resourceManager(resourceManager), provider(std::move(provider)) {}

	typedef std::shared_ptr<InvListWidget> Ptr; ///< Shared widget pointer
	typedef std::shared_ptr<const InvListWidget>
			ConstPtr; ///< Shared constant widget pointer

	void setInventoryList(std::optional<content::InventoryList *> list);

	std::optional<content::InventoryList *> getInventoryList() const {
		return provider.getList(inventoryLocation, listName);
	}

	void rebuild(std::optional<content::InventoryList *> optionalList);
	void onSlotClicked(int i);

	static InvListWidget::Ptr create(InvSelection &selection,
			content::DefinitionManager *defMan,
			ResourceManager *resourceManager,
			content::InventoryProvider provider,
			std::optional<content::InventoryList *> inventoryList = {});

private:
	tgui::Widget::Ptr createToolTip(
			const content::ItemStack &stack, const content::ItemDef *def);
};

} // namespace client
