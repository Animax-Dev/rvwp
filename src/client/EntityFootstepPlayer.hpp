#pragma once

#include "controllers/ICamera.hpp"
#include "types/Controller.hpp"
#include "ResourceManager.hpp"
#include "world/World.hpp"
#include <unordered_map>

namespace client {

class EntityFootstepPlayer : public Controller {
	struct EntityTrackerInfo {
		V3f lastPlayedPosition;
		float lastPlayedTime;
	};

	content::DefinitionManager *definitionManager;
	ResourceManager *resourceManager;
	world::World *world;
	std::unordered_map<int, EntityTrackerInfo> entityTrackerInfo;
	std::vector<std::unique_ptr<sf::Sound>> playingSounds;
	ICamera *camera;
	float elapsedTime = 0;

public:
	EntityFootstepPlayer(content::DefinitionManager *definitionManager,
			ResourceManager *resourceManager, world::World *world,
			ICamera *camera)
			: definitionManager(definitionManager),
			  resourceManager(resourceManager), world(world), camera(camera) {}

	void update(float dtime) override;

private:
	void playFootstep(const V3f &pos);
};

} // namespace client
