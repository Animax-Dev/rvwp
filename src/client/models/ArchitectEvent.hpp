#pragma once

#include "world/Work.hpp"

namespace client {

struct ArchitectEvent {
	enum Type { NEW_WORK, REMOVE_WORK };

	Type type;

	int plotId;
	u32 workId;
	world::Work *work;

	static ArchitectEvent newWork(int plotId, world::Work *work) {
		return {NEW_WORK, plotId, work->id, work};
	}

	static ArchitectEvent removeWork(int plotId, u32 workId) {
		return {REMOVE_WORK, plotId, workId, nullptr};
	}
};

} // namespace client
