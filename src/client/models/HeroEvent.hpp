#pragma once

#include "types/types.hpp"
#include "world/World.hpp"

namespace client {

/// Events fired by local player in hero mode
struct HeroEvent {
	enum Type { INTERACT, DIG, PLACE, SHOW_INVENTORY, PUNCH, RANGED };

	Type type;

	/// Inventory slot
	int slot;

	/// Position
	V3f pos;

	int damage;

	float yaw;

	union {
		world::WorldTile *tile = nullptr;
		world::Entity *entity;
	};

	HeroEvent(Type type, int slot, V3f pos, int damage, float yaw)
			: type(type), slot(slot), pos(pos), damage(damage), yaw(yaw) {}

	HeroEvent(Type type, int slot, V3f pos, int damage, float yaw,
			world::WorldTile *tile)
			: type(type), slot(slot), pos(pos), damage(damage), yaw(yaw),
			  tile(tile) {}

	HeroEvent(Type type, int slot, V3f pos, int damage, float yaw,
			world::Entity *entity)
			: type(type), slot(slot), pos(pos), damage(damage), yaw(yaw),
			  entity(entity) {}

	static HeroEvent interact(V3s pos) {
		return {INTERACT, 0, pos.floating(), 0, 0.f};
	}

	static HeroEvent dig(int slot, V3s pos) {
		return {DIG, slot, pos.floating(), 0, 0.f};
	}

	static HeroEvent place(int slot, V3s pos, world::WorldTile *tile) {
		return {PLACE, slot, pos.floating(), 0, 0.f, tile};
	}

	static HeroEvent showInventory() { return {SHOW_INVENTORY, 0, {}, 0, 0.f}; }

	static HeroEvent punch(int slot, int damage, world::Entity *entity) {
		return {PUNCH, slot, {}, damage, 0.f, entity};
	}

	static HeroEvent ranged(int slot, const V3f &pos, float yaw, int damage,
			world::Entity *entity) {
		return {RANGED, slot, pos, damage, yaw, entity};
	}
};

} // namespace client
