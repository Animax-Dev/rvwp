#pragma once

#include "../../types/Vector.hpp"

namespace client {

struct Camera {
	V3f pos;
	float zoom = 1;
	int focal_z = 0;
};

} // namespace client
