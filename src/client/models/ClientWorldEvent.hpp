#pragma once

namespace client {

struct ClientWorldEvent {
	enum Type {
		RECEIVED_BLOCK,
		ENTITY_NEW,
		ENTITY_UPDATE,
		ENTITY_PROPERTIES,
		ENTITY_DELETE,
		SET_TILE
	};

	Type type;
};

} // namespace client
