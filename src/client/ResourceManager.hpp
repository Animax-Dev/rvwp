#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "content/content.hpp"
#include <string>
#include <vector>
#include <unordered_map>
#include <log.hpp>

namespace client {

class ClientMaterial : public content::Material {
public:
	ClientMaterial() = default;
	explicit ClientMaterial(content::Material &mat)
			: Material(mat.x, mat.y, mat.type, mat.l) {
		name = mat.name;
		w = mat.w;
		h = mat.h;
	}

	sf::Texture *texture = nullptr;

	/// Start of the original texture in `texture`. Will be used for texture
	/// atlas in the future.
	int startx = 0;

	/// Start of the original texture in `texture`. Will be used for texture
	/// atlas in the future.
	int starty = 0;

	/// Get bounding rect of this material in texture
	inline sf::Rect<int> getRectInTexture() const {
		auto tr = getTopRightPosInTexture();
		return {tr.x, tr.y, w, h};
	}

	inline sf::Rect<unsigned int> getURectInTexture() const {
		auto tr = getTopRightPosInTexture();
		return {(unsigned int)tr.x, (unsigned int)tr.y, w, h};
	}

	/// tu and tv of the texture
	inline sf::Vector2i getTopRightPosInTexture() const {
		return {startx + x * w, starty + y * h};
	}

	sf::Sprite toSprite();

	void draw(sf::RenderTarget &target, sf::Vector2f pos);
	inline void draw(sf::RenderTarget &target, int x, int y) {
		draw(target, sf::Vector2f(x, y));
	}
};

class SoundSet {
	std::vector<std::unique_ptr<sf::SoundBuffer>> sounds;

public:
	explicit SoundSet(const std::string &name);

	SoundSet(const SoundSet &other) = delete;

	explicit SoundSet(SoundSet &&other) { sounds = std::move(other.sounds); }

	const sf::SoundBuffer &getRandom() const;

	bool valid() const { return !sounds.empty(); }
};

enum class TextureType { DIFFUSE, NORMAL, BLUEPRINT };

class ResourceManager {
	const sf::Font &defaultFont;
	std::vector<std::unique_ptr<sf::Texture>> textures;
	std::unordered_map<std::string, sf::Texture *> textures_lookup;

	std::unordered_map<std::string, SoundSet> sounds_lookup;

public:
	explicit ResourceManager(const sf::Font &defaultFont)
			: defaultFont(defaultFont) {}
	ResourceManager(const ResourceManager &other) = delete;

	const sf::Font &getDefaultFont() const { return defaultFont; }

	sf::Texture *getTexture(const std::string &name);
	sf::Texture *getTextureOrLoad(const std::string &name);

	sf::Texture *getTileset(TextureType type);

	ClientMaterial getMaterial(content::Material mat);
	ClientMaterial getMaterialOrLoad(content::Material mat);

	const SoundSet *getSound(const std::string &name);
	const SoundSet *getSoundOrLoad(const std::string &name);
};

} // namespace client
