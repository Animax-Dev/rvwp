#include "WorldController.hpp"

using namespace client;

void WorldController::update(float dtime) {
	for (auto e : world.getEntityList()) {
		if (!e->hasAuthority) {
			e->interpolate(dtime);
		}
	}
}
