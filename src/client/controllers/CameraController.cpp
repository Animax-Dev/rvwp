#include "CameraController.hpp"

using namespace client;

CameraController::CameraController(InputController *inputController)
		: input(inputController) {
	auto bind = [&](Action action, std::function<void()> func) {
		inputConnections.emplace_back(input->connect(action, func));
	};

	inputConnections.emplace_back(inputController->connectWithThorContext(
			Action::ZOOM_SCROLL,
			std::bind(&CameraController::onZoom, this, std::placeholders::_1)));

	bind(Action::ZOOM_IN, std::bind(&CameraController::zoom, this, -1));
	bind(Action::ZOOM_OUT, std::bind(&CameraController::zoom, this, 1));
	bind(Action::ZOOM_RESET, std::bind(&CameraController::resetZoom, this));
}

void CameraController::update(float dtime) {
	if (entityToFollow) {
		const V3f pos = entityToFollow->getPosition();

		targetPosition = {pos.x, pos.y, camera.pos.z};
		camera.focal_z = entityToFollow->getPosition().z;
		setCameraZ(camera.pos.z);

		if (pos.z + 1 == entityLastPosition.z && pos.z + 1 == camera.pos.z) {
			changeCameraZ(false);
		}

		entityLastPosition = pos;
	}

	targetPosition.z = camera.pos.z;
	V3f step = (targetPosition - camera.pos) * 0.2f;
	camera.pos += step;
}

void CameraController::zoom(float dist) {
	camera.zoom = clamp(camera.zoom + dist, -5.f, maxZoom);
}

void CameraController::onZoom(thor::ActionContext<Action> context) {
	zoom(-context.event->mouseWheelScroll.delta / 5.f);
}

void CameraController::move(V3f delta) {
	entityToFollow = nullptr;
	targetPosition += delta;
	camera.pos.z = targetPosition.z;
	camera.focal_z = camera.pos.z;
}
