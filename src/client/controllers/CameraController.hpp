#pragma once
#include "types/Controller.hpp"
#include "client/input/InputController.hpp"
#include "../models/Camera.hpp"
#include "ICamera.hpp"
#include "world/World.hpp"
#include <string>

namespace client {

class Game;
class CameraController : public Controller, public ICamera {
	IInput *input;
	Camera camera;
	V3f targetPosition;

	float maxZoom = 1.f;

	world::Entity *entityToFollow = nullptr;
	V3f entityLastPosition;

	std::vector<InputConnection> inputConnections;

public:
	explicit CameraController(InputController *inputController);

	void follow(world::Entity *e) override {
		entityToFollow = e;
		entityLastPosition =
				e ? entityToFollow->getPosition() : V3f().makeInvalid();
	}

	void update(float dtime) override;

	const Camera &getCamera() const override { return camera; }

	void setCameraZ(int z) {
		if (entityToFollow && entityToFollow->getPosition().z > z) {
			camera.pos.z = entityToFollow->getPosition().z;
			targetPosition.z = camera.pos.z;
		} else {
			camera.pos.z = z;
			targetPosition.z = camera.pos.z;
		}
	}

	void changeCameraZ(bool is_up) {
		setCameraZ(camera.pos.z + (is_up ? 1 : -1));
	}

	void zoom(float dist);

	void resetZoom() { camera.zoom = 1; }

	void setMaxZoom(float max) {
		maxZoom = max;
		if (camera.zoom > maxZoom) {
			camera.zoom = maxZoom;
		}
	}

	void move(V3f delta);

	void move(sf::Vector2f delta) override { move(V3f(delta.x, delta.y, 0)); }

	void finishAnimating() { camera.pos = targetPosition; }

private:
	void onZoom(thor::ActionContext<Action> context);
};

} // namespace client
