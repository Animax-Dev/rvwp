#include "ModeController.hpp"
#include "hero/HeroEntityController.hpp"

using namespace client;
using namespace world;

ModeController::ModeController(EventBus<ArchitectEvent> *architectBus,
		EventBus<HeroEvent> *heroBus, EventBus<ClientHeroEvent> *clientBus,
		content::DefinitionManager *def, World *world, IInput *input,
		ICamera *cameraController) {
	architectController = std::make_unique<ArchitectController>(
			architectBus, input, cameraController, def, world);
	addChild(architectController.get());

	heroController = std::make_unique<HeroController>(
			heroBus, clientBus, def, world, input, cameraController);
	addChild(heroController.get());

	setMode(GameMode::HERO);
}

// Required so the destructor has information about the otherwise incomplete
// types
ModeController::~ModeController() = default;

void ModeController::update(float dtime) {}

void ModeController::setMode(GameMode newMode) {
	mode = newMode;

	switch (mode) {
	case HERO:
		architectController->disable();
		heroController->enable();
		break;
	case ARCHITECT:
		heroController->disable();
		architectController->enable();
		break;
	}
}
