#pragma once

#include "Client.hpp"
#include "models/ArchitectEvent.hpp"
#include "models/HeroEvent.hpp"

#include "events/EventBus.hpp"
#include "events/EventListener.hpp"
#include "content/InventoryCommand.hpp"

namespace client {

/// Listens to various event buses, and calls methods on the client.
class ClientEventListener {
	Client *client;
	const EventListener<ArchitectEvent> architectListener;
	const EventListener<HeroEvent> heroListener;
	const EventListener<content::InventoryCommand> inventoryListener;

public:
	ClientEventListener(Client *client, EventBus<ArchitectEvent> *architectBus,
			EventBus<HeroEvent> *heroBus,
			EventBus<content::InventoryCommand> *inventoryBus)
			: client(client),
			  architectListener(architectBus,
					  [this](auto e) { handleArchitectEvent(e); }),
			  heroListener(heroBus, [this](auto e) { handleHeroEvent(e); }),
			  inventoryListener(inventoryBus,
					  [this](auto e) { handleInventoryEvent(e); }) {}

	void handleArchitectEvent(const ArchitectEvent &event);
	void handleHeroEvent(const HeroEvent &event);
	void handleInventoryEvent(const content::InventoryCommand &command);
};

} // namespace client
