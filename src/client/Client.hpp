#pragma once
#include "models/ClientHeroEvent.hpp"
#include "../events/EventBus.hpp"
#include "../network/protocol.hpp"
#include "../network/net.hpp"
#include "../network/packets.hpp"
#include "world/World.hpp"
#include "content/Inventory.hpp"
#include "content/InventoryCommand.hpp"

namespace client {

class Game;
class Client {
	network::Socket socket;
	network::Address server;
	float player_send_counter = 0;

	world::Entity *localPlayer = nullptr;
	std::unordered_map<int, int> workServerToClient;
	std::unordered_map<int, int> workClientToServer;

	Game *game;
	EventBus<ClientHeroEvent> *heroBus;

	std::string username;

	enum State { CONNECTING, LOADING, LOADED, DISCONNECTED };
	State state = DISCONNECTED;

public:
	Client(Game *game, EventBus<ClientHeroEvent> *heroBus);
	Client(const Client &other) = delete;
	~Client();

	/// Network Functions. Call before running run()
	bool connect(network::Address address, const std::string &username,
			const std::string &password);

	void disconnect();

	void initPacketHandlers();

	bool isConnected() const { return state == LOADED; }

	/// Receives packets
	void update(float dtime);

	// Handle packets
	void handleHello(network::Packet &pkt);
	void handleReady(network::Packet &pkt);
	void handleDisconnect(network::Packet &pkt);
	void handleItemDefinition(network::Packet &pkt);
	void handleToolDefinition(network::Packet &pkt);
	void handleProfile(network::Packet &pkt);
	void handleInventory(network::Packet &pkt);
	void handleChatMessage(network::Packet &pkt);
	void handleSetTime(network::Packet &pkt);
	void handleWorldBlock(network::Packet &pkt);
	void handleWorldEntityNew(network::Packet &pkt);
	void handleWorldEntityUpdate(network::Packet &pkt);
	void handleWorldEntityProperties(network::Packet &pkt);
	void handleWorldEntityDelete(network::Packet &pkt);
	void handleWorldGoto(network::Packet &pkt);
	void handleWorldSetTile(network::Packet &pkt);
	void handleWorldPlot(network::Packet &pkt);
	void handleWorldNewWork(network::Packet &pkt);
	void handleWorldRemoveWork(network::Packet &pkt);
	void handleSpawnParticle(network::Packet &pkt);

	// Sending packets
	void sendChatMessage(const std::string &message);
	void sendPlayerInventoryCommand(const content::InventoryCommand &command);
	void sendPlayerPosition();
	void sendSetTile(
			const V3s &pos, const world::WorldTile *tile, int hotbar_idx);
	void sendTileInteract(const V3s &pos);
	void sendPunchEntity(
			const world::Entity *entity, u8 hotbar_idx, u16 damage_done);
	void sendWorkCreated(int plotId, world::Work *work);

	inline void getStats(network::NetworkStats &stats) {
		socket.getStats(stats);
	}
	void sendWorkRemoved(u32 workClientId);
	void sendFireWeapon(const V3f &pos, float yaw, u8 hotbar_idx);
};

} // end namespace client
