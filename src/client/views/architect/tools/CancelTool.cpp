#include "CancelTool.hpp"

#include "client/controllers/architect/ArchitectController.hpp"

using namespace client;
using namespace content;

CancelTool::CancelTool(ArchitectController *controller, const ToolSpec &spec)
		: controller(controller), spec(spec),
		  selectHelper(spec.selectType, spec.fixedSelectionSize, controller,
				  sf::Color(0xFF3333FF)) {
	auto bind = [&](Action action, std::function<void()> func) {
		inputConnections.emplace_back(
				controller->getInput()->connect(action, func));
	};

	bind(Action::TOOL_SELECT, [&]() { onSelect(); });
	bind(Action::TOOL_RELEASE, [&]() { onDeselect(); });
}

void CancelTool::onSelect() {
	selectHelper.start();
}

void CancelTool::onDeselect() {
	for (const auto &rect : selectHelper.getSelectedRects()) {
		controller->cancelWork(rect.minPos, rect.getMaxPos() - V3s(1, 1, 1));
	}

	selectHelper.stop();
}
