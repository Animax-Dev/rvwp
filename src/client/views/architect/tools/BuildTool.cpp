#include "BuildTool.hpp"
#include "client/controllers/architect/ArchitectController.hpp"

using namespace client;
using namespace content;

BuildTool::BuildTool(ArchitectController *controller,
		const content::TileDef *def, const ToolSpec &spec)
		: controller(controller), def(def), spec(spec),
		  selectHelper(spec.selectType, spec.fixedSelectionSize, controller,
				  sf::Color::White) {
	auto bind = [&](Action action, std::function<void()> func) {
		inputConnections.emplace_back(
				controller->getInput()->connect(action, func));
	};

	bind(Action::TOOL_SELECT, [&]() { onSelect(); });
	bind(Action::TOOL_RELEASE, [&]() { onDeselect(); });
}

void BuildTool::onSelect() {
	selectHelper.start();
}

void BuildTool::onDeselect() {
	for (const auto &rect : selectHelper.getSelectedRects()) {
		controller->addBuildWork(
				rect.minPos, rect.getMaxPos() - V3s(1, 1, 1), def, spec.layer);
	}

	selectHelper.stop();
}
