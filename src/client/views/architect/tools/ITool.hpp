#pragma once

#include "client/views/View.hpp"
#include "content/ToolSpec.hpp"

namespace client {
class ITool : public View {
public:
	virtual const content::ToolSpec &getSpec() = 0;
};
} // namespace client
