#include "DeconstructTool.hpp"
#include "client/controllers/architect/ArchitectController.hpp"

using namespace client;
using namespace content;

DeconstructTool::DeconstructTool(
		ArchitectController *controller, const ToolSpec &spec)
		: controller(controller), spec(spec),
		  selectHelper(spec.selectType, spec.fixedSelectionSize, controller,
				  sf::Color::White) {
	auto bind = [&](Action action, std::function<void()> func) {
		inputConnections.emplace_back(
				controller->getInput()->connect(action, func));
	};

	bind(Action::TOOL_SELECT, [&]() { onSelect(); });
	bind(Action::TOOL_RELEASE, [&]() { onDeselect(); });
}

void DeconstructTool::onSelect() {
	selectHelper.start();
}

void DeconstructTool::onDeselect() {
	for (const auto &rect : selectHelper.getSelectedRects()) {
		controller->addDeconstructWork(
				rect.minPos, rect.getMaxPos() - V3s(1, 1, 1), spec);
	}

	selectHelper.stop();
}
