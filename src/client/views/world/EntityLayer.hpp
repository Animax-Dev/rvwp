#pragma once

#include "RenderLayer.hpp"

namespace client {

class EntityLayer : public IWorldRenderLayer {
	ResourceManager *resources;

public:
	explicit EntityLayer(ResourceManager *resources)
			: IWorldRenderLayer("Entity"), resources(resources) {}

protected:
	void draw_impl(sf::RenderTarget &target, sf::RenderStates states,
			const client::RenderZLevel &level,
			const sf::FloatRect &cameraRect) override;
};

} // namespace client
