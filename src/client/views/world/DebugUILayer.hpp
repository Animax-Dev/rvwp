#pragma once

#include "WorldView.hpp"

namespace client {

class DebugUILayer : public IWorldRenderLayer {
	const sf::Font &font;

public:
	DebugUILayer(const sf::Font &font)
			: IWorldRenderLayer("DebugUI"), font(font) {}

	void enable() override;
	void disable() override;

protected:
	void draw_impl(sf::RenderTarget &target, sf::RenderStates states,
			const RenderZLevel &level,
			const sf::FloatRect &cameraRect) override;
};

} // namespace client
