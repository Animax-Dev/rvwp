#include "ParticleLayer.hpp"

using namespace client;

ParticleLayer::ParticleLayer(ResourceManager *resources, World *world)
		: IWorldRenderLayer("Particle"), resources(resources), world(world) {
	particleSystem.setTexture(*resources->getTextureOrLoad("fire.png"));

	particleSystem.addEmitter(
			[this](thor::EmissionInterface &system, sf::Time dt) {
				singleParticleEmitter(system, dt);
			});

	particleSystem.addAffector([this](thor::Particle &particle, sf::Time time) {
		sf::Vector2f sfPos = particle.position / (float)TILE_SIZE;
		sf::Vector2f sfVelocity = particle.velocity / (float)TILE_SIZE;
		V3f pos(sfPos.x, sfPos.y, 0);
		V3f nextPos =
				pos + V3f(sfVelocity.x, sfVelocity.y, 0) * time.asSeconds();
		if (this->world->collidesAt(pos, nextPos)) {
			abandonParticle(particle);
		}
	});
}

void ParticleLayer::addSingleParticle(const V3f &pos, const V3f &velocity,
		const content::Material &material) {
	singleParticleEmitter.push(sf::Vector2f(pos.x, pos.y) * (float)TILE_SIZE,
			velocity * (float)TILE_SIZE, 10, 0, 0);
}

void ParticleLayer::draw_impl(sf::RenderTarget &target, sf::RenderStates states,
		const client::RenderZLevel &level, const sf::FloatRect &cameraRect) {
	target.draw(particleSystem);
}
