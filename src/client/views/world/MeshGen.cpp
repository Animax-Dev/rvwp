#include "MeshGen.hpp"
#include "world/World.hpp"
#include "content/content.hpp"
#include "client/ResourceManager.hpp"

const sf::Vector2u tileSize(TILE_SIZE, TILE_SIZE);

using namespace world;
using namespace client;

const u8 BITS8_LUT[256] = {0, 47, 1, 1, 47, 47, 1, 1, 2, 2, 3, 4, 2, 2, 3, 4, 5,
		5, 6, 6, 5, 5, 7, 7, 8, 8, 9, 10, 8, 8, 11, 12, 47, 47, 1, 1, 47, 47, 1,
		1, 2, 2, 3, 4, 2, 2, 3, 4, 5, 5, 6, 6, 5, 5, 7, 7, 8, 8, 9, 10, 8, 8,
		11, 12, 13, 13, 14, 14, 13, 13, 14, 14, 15, 15, 16, 17, 15, 15, 16, 17,
		18, 18, 19, 19, 18, 18, 20, 20, 21, 21, 22, 23, 21, 21, 24, 25, 13, 13,
		14, 14, 13, 13, 14, 14, 26, 26, 27, 28, 26, 26, 27, 28, 18, 18, 19, 19,
		18, 18, 20, 20, 29, 29, 30, 31, 29, 29, 32, 33, 47, 47, 1, 1, 47, 47, 1,
		1, 2, 2, 3, 4, 2, 2, 3, 4, 5, 5, 6, 6, 5, 5, 7, 7, 8, 8, 9, 10, 8, 8,
		11, 12, 47, 47, 1, 1, 47, 47, 1, 1, 2, 2, 3, 4, 2, 2, 3, 4, 5, 5, 6, 6,
		5, 5, 7, 7, 8, 8, 9, 10, 8, 8, 11, 12, 13, 13, 14, 14, 13, 13, 14, 14,
		15, 15, 16, 17, 15, 15, 16, 17, 34, 34, 35, 35, 34, 34, 36, 36, 37, 37,
		38, 39, 37, 37, 40, 41, 13, 13, 14, 14, 13, 13, 14, 14, 26, 26, 27, 28,
		26, 26, 27, 28, 34, 34, 35, 35, 34, 34, 36, 36, 42, 42, 43, 44, 42, 42,
		45, 46};

const u8 BITS8_AFFECT_LUT[8] = {64, 32, 8, 1, 2, 4, 16, 128};

#define SET_PIXEL(x, y, r, g, b)                                               \
	{                                                                          \
		u8 *ptr = &pixels[((x) + (y)*LIGHTMAP_SIZE) * 4];                      \
		*(ptr++) = (r);                                                        \
		*(ptr++) = (g);                                                        \
		*(ptr++) = (b);                                                        \
		*(ptr++) = 0xFF;                                                       \
	}

inline int map(int x) {
	return (x > 0) ? CHUNK_SIZE : -1;
}

inline int toRange(int x) {
	return (x + CHUNK_SIZE) % CHUNK_SIZE;
}

const u8 LIGHT_LUT[] = {
		0, 4, 9, 14, 20, 27, 36, 45, 56, 70, 86, 106, 131, 162, 202, 255};

void MeshGen::updateLightMapTexture(
		sf::Texture &texture, world::WorldChunk *chunk) {
	std::array<sf::Uint8, LIGHTMAP_SIZE * LIGHTMAP_SIZE * 4> pixels{};

	for (int rx = 0; rx < CHUNK_SIZE; ++rx) {
		for (int ry = 0; ry < CHUNK_SIZE; ++ry) {
			u8 artificial = LIGHT_LUT[chunk->getArtificial(rx, ry)];
			u8 sunlight = LIGHT_LUT[chunk->getSunlight(rx, ry)];

			int px = (rx + 1) * 2;
			int py = (ry + 1) * 2;

			// Current
			SET_PIXEL(px, py, artificial, artificial, artificial)
			SET_PIXEL(px, py + 1, sunlight, sunlight, sunlight)
			SET_PIXEL(px + 1, py, 0, 0, 0)
			SET_PIXEL(px + 1, py + 1, 0, 0, 0)
		}
	}

	for (const auto &dir : NEIGHBOURS_4) {
		WorldChunk *neighbour =
				world->getChunk(chunk->pos + V3s(dir.x, dir.y, 0));
		if (!neighbour) {
			continue;
		}

		V3s start = V3s(map(dir.x), map(dir.y), 0);
		V3s delta = (dir.x != 0) ? V3s(0, 1, 0) : V3s(1, 0, 0);

		for (int i = 0; i < CHUNK_SIZE; i++) {
			V3s current = start + delta * i;

			int rx = toRange(current.x);
			int ry = toRange(current.y);

			u8 artificial = LIGHT_LUT[neighbour->getArtificial(rx, ry)];
			u8 sunlight = LIGHT_LUT[neighbour->getSunlight(rx, ry)];

			int px = (current.x + 1) * 2;
			int py = (current.y + 1) * 2;
			SET_PIXEL(px, py, artificial, artificial, artificial)
			SET_PIXEL(px, py + 1, sunlight, sunlight, sunlight)
		}
	}

	texture.update(&pixels[0]);
}

void MeshGen::spreadConnections(ChunkConnectionMap &out, WorldTile *tiles) {
	for (unsigned int j = 0; j < CHUNK_SIZE; ++j) {
		for (unsigned int i = 0; i < CHUNK_SIZE; ++i) {
			const WorldTile &tile = tiles[i + j * CHUNK_SIZE];
			if (!tile) {
				continue;
			}

			content::TileDef *tdef = def->getTileDef(tile.cid);
			if (!tdef ||
					tdef->material.type != content::EMatType::EMT_CON_8BIT) {
				continue;
			}

			for (u8 dirI = 0; dirI < 8; dirI++) {
				const auto &offset = NEIGHBOURS_8[dirI];
				int ri = i + offset.x;
				int rj = j + offset.y;
				if (ri >= 0 && ri < CHUNK_SIZE && rj >= 0 && rj < CHUNK_SIZE) {
					out[ri + rj * CHUNK_SIZE] |= BITS8_AFFECT_LUT[dirI];
				}
			}
		}
	}
}

void MeshGen::buildFloorMesh(
		sf::VertexArray &floorMesh, world::WorldChunk *chunk) {
	// Initialise vertex arrays
	floorMesh.clear();
	floorMesh.setPrimitiveType(sf::Quads);
	floorMesh.resize(CHUNK_SIZE * CHUNK_SIZE * 4);

	// Build meshes
	for (unsigned int j = 0; j < CHUNK_SIZE; ++j) {
		for (unsigned int i = 0; i < CHUNK_SIZE; ++i) {
			int tu = 0;
			int tv = 0;

			// Check for floor
			const WorldTile &tile = chunk->floort[i + j * CHUNK_SIZE];
			if (tile) {
				content::TileDef *tdef = def->getTileDef(tile.cid);
				if (tdef) {
					ClientMaterial cmat =
							resourceManager->getMaterial(tdef->material);
					tu = cmat.startx + cmat.x;
					tv = cmat.starty + cmat.y;
				} else {
					LogF(ERROR, "[MeshGen] No such tiledef, %d", tile.cid);
				}
			} else {
				content_id cid = chunk->terrain[i + j * CHUNK_SIZE];
				if (cid == 0) {
					LogF(INFO,
							"[MeshGen] content ignore found at (%d, %d idx "
							"%d)",
							i, j, (i + j * CHUNK_SIZE));
				}

				content::TileDef *tdef = def->getTileDef(cid);
				if (tdef) {
					ClientMaterial cmat =
							resourceManager->getMaterial(tdef->material);
					tu = cmat.startx + cmat.x;
					tv = cmat.starty + cmat.y;
				} else {
					LogF(ERROR, "[MeshGen] ERROR! No such floor def, %d", cid);
					tu = cid;
				}
			}

			// Create quad
			sf::Vertex *quad = &floorMesh[(i + j * CHUNK_SIZE) * 4];
			setQuad(quad, i, j, tu, tv);
		}
	}
}

void MeshGen::buildTileMesh(sf::VertexArray &out, world::WorldTile *tiles,
		const V3s &origin, const ChunkConnectionMap &bits) {
	out.clear();
	out.setPrimitiveType(sf::Quads);

	// Build meshes
	for (unsigned int j = 0; j < CHUNK_SIZE; ++j) {
		for (unsigned int i = 0; i < CHUNK_SIZE; ++i) {
			// Tiles
			{
				const WorldTile &tile = tiles[i + j * CHUNK_SIZE];
				if (tile) {
					int tu = 0;
					int tv = 0;

					content::TileDef *tdef = def->getTileDef(tile.cid);
					if (tdef) {
						ClientMaterial cmat =
								resourceManager->getMaterial(tdef->material);
						if (cmat.type == content::EMT_DOORLIKE) {
							WorldTile *up =
									world->get(origin + V3s(i, j - 1, 0),
											ChunkLayer::Tile);
							WorldTile *down =
									world->get(origin + V3s(i, j + 1, 0),
											ChunkLayer::Tile);
							if (up || down) {
								tv = cmat.starty + cmat.y;
								tu = cmat.startx + cmat.x;
							} else {
								tv = cmat.starty + cmat.y;
								tu = cmat.startx + cmat.x + 1;
							}
						} else if (cmat.type == content::EMT_CON_8BIT) {
							char idx = BITS8_LUT[bits[i + j * CHUNK_SIZE]];
							auto width = 6;
							tv = cmat.starty + cmat.y + idx / width;
							tu = cmat.startx + cmat.x + idx % width;
						} else {
							tv = cmat.starty + cmat.y;
							tu = cmat.startx + cmat.x;
						}
					} else {
						LogF(ERROR, "[MeshGen] Unknown tile %d %d, %d",
								tile.cid, i, j);
					}

					sf::Vertex quad[4];
					setQuad(quad, i, j, tu, tv);
					out.append(quad[0]);
					out.append(quad[1]);
					out.append(quad[2]);
					out.append(quad[3]);
				}
			}
		}
	}
}

void MeshGen::setQuad(sf::Vertex *quad, int x, int y, int tu, int tv) {
	// define its 4 corners
	{
		int l = x * tileSize.x;
		int t = y * tileSize.y;
		int r = (x + 1) * tileSize.x;
		int b = (y + 1) * tileSize.y;

		quad[0].position = sf::Vector2f(l, t);
		quad[1].position = sf::Vector2f(r, t);
		quad[2].position = sf::Vector2f(r, b);
		quad[3].position = sf::Vector2f(l, b);
	}

	// define its 4 texture coordinates
	{
		int l = tu * tileSize.x;
		int t = tv * tileSize.y;
		int r = (tu + 1) * tileSize.x;
		int b = (tv + 1) * tileSize.y;

		float offset = 0.015f;
		quad[0].texCoords = sf::Vector2f(l, t) + sf::Vector2f(offset, offset);
		quad[1].texCoords = sf::Vector2f(r, t) + sf::Vector2f(-offset, offset);
		quad[2].texCoords = sf::Vector2f(r, b) + sf::Vector2f(-offset, -offset);
		quad[3].texCoords = sf::Vector2f(l, b) + sf::Vector2f(offset, -offset);
	}
}
