#include "StandardLayer.hpp"

using namespace client;

void StandardLayer::draw_impl(sf::RenderTarget &target, sf::RenderStates states,
		const client::RenderZLevel &level, const sf::FloatRect &cameraRect) {
	for (const auto &r : level.peek()) {
		sf::FloatRect chunk_rect(r.getPos().x * CHUNK_SIZE * TILE_SIZE,
				r.getPos().y * CHUNK_SIZE * TILE_SIZE, CHUNK_SIZE * TILE_SIZE,
				CHUNK_SIZE * TILE_SIZE);
		if (!cameraRect.intersects(chunk_rect)) {
			// LogF(ERROR, "No intersection! %d %d %d %d and %d %d %d
			// %d", 	crect.left, crect.top, crect.width,
			// crect.height, camera_view.getCenter)
			continue;
		}

		sf::Transform tf;
		tf.translate(r.getPos().x * CHUNK_SIZE * TILE_SIZE,
				r.getPos().y * CHUNK_SIZE * TILE_SIZE);
		states.transform = tf;
		worldShader.setUniform("lightmap", r.lightMapTexture);
		target.draw(r.floorMesh, states);
		target.draw(r.tilesMesh, states);
	}
}
