#include "LightDebugLayer.hpp"

using namespace client;

void LightDebugLayer::draw_impl(sf::RenderTarget &target,
		sf::RenderStates states, const client::RenderZLevel &level,
		const sf::FloatRect &cameraRect) {
	sf::Text text("", font, 16);
	text.setFillColor(sf::Color::White);

	sf::RectangleShape text_bg;
	text_bg.setFillColor(sf::Color(0, 0, 0, 128));

	sf::RenderStates statesLighting;
	for (const auto &r : level.peek()) {
		sf::FloatRect chunk_rect(r.getPos().x * CHUNK_SIZE * TILE_SIZE,
				r.getPos().y * CHUNK_SIZE * TILE_SIZE, CHUNK_SIZE * TILE_SIZE,
				CHUNK_SIZE * TILE_SIZE);
		if (!cameraRect.intersects(chunk_rect)) {
			continue;
		}

		sf::Sprite lightmap(r.lightMapTexture);
		lightmap.setPosition(r.getPos().x * CHUNK_SIZE * TILE_SIZE,
				r.getPos().y * CHUNK_SIZE * TILE_SIZE);
		lightmap.setScale(TILE_SIZE / 2, TILE_SIZE / 2);
		lightmap.setTextureRect({2, 2, 32, 32});
		target.draw(lightmap);

		for (int y = 0; y < CHUNK_SIZE; y++) {
			for (int x = 0; x < CHUNK_SIZE; x++) {
				std::ostringstream os;
				auto light = r.chunk->lightLevels[x + y * CHUNK_SIZE];
				os << std::hex << (int)GetSunlight(light) << " - "
				   << (int)GetArtificialRed(light);
				text.setString(os.str());
				sf::FloatRect textRect = text.getLocalBounds();
				text.setOrigin(textRect.left + textRect.width / 2.0f,
						textRect.top + textRect.height / 2.0f);
				text.setPosition(
						(r.getPos().x * CHUNK_SIZE + x + 0.5f) * TILE_SIZE,
						(r.getPos().y * CHUNK_SIZE + y + 0.5f) * TILE_SIZE);

				sf::Vector2f size(text.getLocalBounds().width + 8,
						text.getLocalBounds().height + 8);
				text_bg.setPosition(text.getPosition());
				text_bg.setSize(size);
				text_bg.setOrigin(size / 2.f);
				target.draw(text_bg);

				target.draw(text);
			}
		}
	}
}
