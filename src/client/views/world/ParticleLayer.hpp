#pragma once

#include <Thor/Particles.hpp>
#include <types/Controller.hpp>
#include "RenderLayer.hpp"
#include "client/sfext/SingleParticleEmitter.hpp"
#include "client/ResourceManager.hpp"
#include "world/World.hpp"

namespace client {

class ParticleLayer : public IWorldRenderLayer {
	ResourceManager *resources;
	World *world;
	thor::ParticleSystem particleSystem;
	sfext::SingleParticleEmitter singleParticleEmitter;

public:
	ParticleLayer(ResourceManager *resources, World *world);

	void addSingleParticle(const V3f &pos, const V3f &velocity,
			const content::Material &material);

	void update(float dtime) { particleSystem.update(sf::seconds(dtime)); }

protected:
	void draw_impl(sf::RenderTarget &target, sf::RenderStates states,
			const client::RenderZLevel &level,
			const sf::FloatRect &cameraRect) override;
};

} // namespace client
