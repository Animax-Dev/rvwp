#pragma once

#include "WorldView.hpp"

namespace client {

class LightDebugLayer : public IWorldRenderLayer {
	const sf::Font &font;

public:
	LightDebugLayer(const sf::Font &font)
			: IWorldRenderLayer("LightDebug"), font(font) {}

protected:
	void draw_impl(sf::RenderTarget &target, sf::RenderStates states,
			const RenderZLevel &level,
			const sf::FloatRect &cameraRect) override;
};

} // namespace client
