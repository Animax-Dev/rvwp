#include "ChatSendView.hpp"
#include "client/Game.hpp"
#include <sstream>

using namespace client;

bool ChatSendView::load() {
	window = tgui::ChildWindow::create("Chat Message");
	window->setPosition("&.width / 2 - width / 2", "&.height / 2 - height / 2");
	window->setClientSize({400, 66});
	window->setPositionLocked(true);
	window->setTitleTextSize(17);
	gui.add(window);

	auto close = [&]() {
		game->guiExit();
		return true;
	};
	window->onEscapeKeyPress(close);
	window->onClose(close);

	txtMessage = tgui::EditBox::create();
	window->add(txtMessage);
	txtMessage->setPosition({10, 10});
	txtMessage->setSize("100% - 100", "45");
	txtMessage->onReturnKeyPress([&]() { onSendClick(); });

	auto button = tgui::Button::create("Send");
	window->add(button);
	button->setPosition("100% - width - 10", "10");
	button->setSize(80, 45);
	button->onPress(&ChatSendView::onSendClick, this);

	window->setVisible(false);

	return true;
}

void ChatSendView::activate() {
	window->setVisible(true);

	gui.unfocusAllWidgets();
	txtMessage->setFocused(true);
}

void ChatSendView::deactivate() {
	window->setVisible(false);
}

void ChatSendView::onSendClick() {
	std::string msg = txtMessage->getText().toStdString();
	game->say(msg);
	txtMessage->setText("");
	game->guiExit();
}

void ChatSendView::setText(const std::string &text) {
	txtMessage->setText(text);
	txtMessage->selectText(text.size(), 0);
}
