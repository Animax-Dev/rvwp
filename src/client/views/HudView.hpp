#pragma once
#include <SFML/Graphics.hpp>
#include <memory>

#include "client/sfext/BarLineGraph.hpp"
#include "client/sfext/ChatView.hpp"
#include "client/sfext/DropShadow.hpp"

#include "content/content.hpp"
#include "content/Inventory.hpp"
#include "client/controllers/WorldController.hpp"
#include "client/ResourceManager.hpp"
#include "View.hpp"
#include <RichText.hpp>

namespace client {

enum class HudGraphType {
	GRAPH_DTIME,
	GRAPH_PACKETS_SEND,
	GRAPH_PACKETS_RECV,
	GRAPH_PACKETS_LOST,
	GRAPH_PACKETS_RELIABLE,
	GRAPH_CHUNK_MESHGEN,
	GRAPH_FPS
};

class Game;
class WorldView;
class HudView : public View {
	Game *game;
	const sf::Font &font;
	WorldView *worldView;

	sf::Text hud_debug_info;
	float display_refresh_counter = 0;
	float last_dtime = 0;
	sfext::ChatView chat;
	std::unique_ptr<sfext::DropShadow> chatShadow;
	sf::Vector2i bottomRight;

	int debug_mode =
#ifdef NDEBUG
			0;
#else
			1;
#endif

public:
	using Ptr = std::shared_ptr<HudView>;

	HudView(Game *game, const sf::Font &font, WorldView *worldView);

	void pushStat(HudGraphType type, int value);

	bool load() override;
	void draw(sf::RenderTarget &target, sf::RenderStates states) override {}
	void drawHUD(sf::RenderTarget &target, sf::RenderStates states) override;
	void update(float delta) override;
	void resize(unsigned int width, unsigned int height) override;

	void pushChatMessage(const std::string &msg) {
		chat.pushChatMessage(msg);
		chatShadow = nullptr;
	}

	void switchDebugMode();

	void setViewBoxing(int bottom) { bottomRight.y = bottom; }

	void pushStatDTime(float dtime) {
		pushStat(HudGraphType::GRAPH_DTIME, (int)(dtime * 1000000.f));
	}

	void pushStatPacketSend(int packet_send) {
		pushStat(HudGraphType::GRAPH_PACKETS_SEND, packet_send);
	}

	void pushStatPacketRecv(int packets) {
		pushStat(HudGraphType::GRAPH_PACKETS_RECV, packets);
	}

	void pushStatPacketLost(int packets) {
		pushStat(HudGraphType::GRAPH_PACKETS_LOST, packets);
	}

	void pushStatPacketReliable(int value) {
		pushStat(HudGraphType::GRAPH_PACKETS_RELIABLE, value);
	}

	void pushStatFPS(float fps) {
		pushStat(HudGraphType::GRAPH_FPS, (int)(fps));
	}

	void pushStatChunkGen(int chunks) {
		pushStat(HudGraphType::GRAPH_CHUNK_MESHGEN, chunks);
	}

private:
	std::vector<BarLineGraph> graphs;
	BarLineGraph fps_graph;
};

} // namespace client
