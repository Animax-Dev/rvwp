#pragma once

#include <stdexcept>

#define SanitySTR(x) #x
#define FatalError(message) \
	throw std::runtime_error(message " at " __FILE__ ":" SanitySTR(__LINE__));

#define SanityCheck(cond) \
	if (!(cond)) { FatalError(#cond); }
