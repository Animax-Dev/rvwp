#pragma once

#define LOGURU_WITH_STREAMS 1
#define LOGURU_THREADNAME_WIDTH 6
#include <loguru.hpp>

#define Log(sys, verbosity) LOG_S(verbosity) << "[" << (sys) << "] "
#define LogF LOG_F
