#pragma once

#include "Entity.hpp"
#include <cassert>

namespace world {

class EntityPhysicsController {
	World *world;
	Entity *entity;
	V3f moveVector;
	V3f velocity;

public:
	explicit EntityPhysicsController(World *world, Entity *entity)
			: world(world), entity(entity) {
		assert(entity);
	}

	inline void setMoveVector(V3f v) { moveVector = v; }

	void update(float dtime);
};

} // namespace world
