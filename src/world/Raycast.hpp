#pragma once
#include "World.hpp"
#include "SelectionSpec.hpp"
#include "../types/types.hpp"

#include <log.hpp>
#include <cassert>

namespace world {

class Raycast {
	World *world;
	Entity *ignoredEntity;

public:
	bool entity_collision = true;
	bool tile_collision = true;

	explicit Raycast(World *world, Entity *ignoredEntity = nullptr)
			: world(world), ignoredEntity(ignoredEntity) {}

	SelectionSpec trace(V3f from, V3f direction, float max_distance,
			float initial_jump_radius = 0);

	inline SelectionSpec trace_to(V3f from, V3f to,
			float initial_jump_radius = 0,
			float max_distance = 100000000000.f) {
		assert(to.z == from.z);

		V3f delta = to - from;
		if (delta.isZero()) {
			return {};
		}

		float distance = std::min(delta.length(), max_distance);

		V3f direction = delta;
		direction.normalise2();
		return trace(from, direction, distance, initial_jump_radius);
	}

	inline SelectionSpec trace_yaw(V3f from, float yaw, float max_distance,
			float initial_jump_radius = 0) {
		return trace(from, V3f(cos(yaw), sin(yaw), 0), max_distance,
				initial_jump_radius);
	}
};

} // namespace world
