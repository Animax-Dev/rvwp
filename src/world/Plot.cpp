#include "Plot.hpp"
#include "World.hpp"

using namespace world;

bool Plot::contains(const V3s &pos) const {
	if (!bounds.contains(pos)) {
		return false;
	}

	for (const auto &box : boxes) {
		if (box.contains(pos)) {
			return true;
		}
	}

	return false;
}

bool Plot::intersects(const Rect3s &rect) const {
	if (!bounds.intersects(rect)) {
		return false;
	}

	for (const auto &box : boxes) {
		if (box.intersects(rect)) {
			return true;
		}
	}

	return false;
}

Work *Plot::getWork(u32 workId) {
	for (auto &work : works) {
		if (work->id == workId) {
			return work.get();
		}
	}
	return nullptr;
}

bool Plot::hasWork(u32 workId) {
	return getWork(workId);
}

void Plot::getAllWork(std::vector<Work *> &out) const {
	for (auto &e : works) {
		out.push_back(e.get());
	}
}

void Plot::getWorkInBounds(std::vector<Work *> &out, const Rect3s &rect) {
	for (auto &e : works) {
		if (e->intersects(rect)) {
			out.push_back(e.get());
		}
	}
}

Work *Plot::addWorkRaw(std::unique_ptr<Work> &&work) {
	if (!bounds.contains(work->bounds)) {
		return nullptr;
	}

	for (auto &e : works) {
		if (e->layer == work->layer && e->intersects(work->bounds)) {
			return nullptr;
		}
	}

	auto ptr = work.get();

	works.emplace_back(std::move(work));

	return ptr;
}

bool Plot::removeWorkRaw(u32 workId) {
	for (auto it = works.begin(); it != works.end(); it++) {
		if ((*it)->id == workId) {
			works.erase(it);
			return true;
		}
	}
	return false;
}
