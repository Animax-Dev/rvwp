#pragma once

#include "types/types.hpp"
#include "content/Material.hpp"
#include "Chunk.hpp"

namespace world {

/// Represents a pending task to be done by an NPC or player
class Work {
public:
	u32 id = 0;
	std::string type;
	Rect3s bounds;
	content::Material material;

	std::string itemName;
	world::ChunkLayer layer;

	Work(const std::string &type, const Rect3s &bounds,
			const content::Material &material, const std::string &itemName = "",
			world::ChunkLayer layer = world::ChunkLayer::Tile)
			: type(type), bounds(bounds), material(material),
			  itemName(itemName), layer(layer) {}

	V3s getPosition() const { return bounds.minPos; }

	bool intersects(const Rect3s &other) const {
		return bounds.intersects(other);
	}
};

} // namespace world
