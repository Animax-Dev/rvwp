#pragma once

#include "../types/Vector.hpp"
#include "../types/Rect.hpp"

#include "Work.hpp"

#include <utility>
#include <memory>
#include <vector>
#include <cassert>

namespace world {

class World;
class Plot {
	std::vector<Rect3s> boxes;
	std::vector<std::unique_ptr<Work>> works;

public:
	const int id;
	std::string name;
	Rect3s bounds;

	Plot(int id, std::string name, Rect3s bounds)
			: id(id), name(std::move(name)), bounds() {
		if (!bounds.empty()) {
			addBox(bounds);
		}
	}

	Plot(const Plot &plot) = delete;

	bool contains(const V3s &pos) const;

	bool intersects(const Rect3s &rect) const;

	inline const std::vector<Rect3s> &peekBoxes() const { return boxes; }

	void addBox(const Rect3s &box) {
		assert(!box.empty());
		boxes.push_back(box);
		bounds.extend(box);
	}

	void clearBoxes() {
		boxes.clear();
		bounds = Rect3s();
	}

	void addBoxes(const std::vector<Rect3s> &toAdd) {
		boxes.insert(boxes.end(), toAdd.begin(), toAdd.end());
		for (const auto &box : toAdd) {
			assert(!box.empty());
			bounds.extend(box);
		}
	}

	Work *getWork(u32 workId);
	bool hasWork(u32 workId);
	void getAllWork(std::vector<Work *> &out) const;
	void getWorkInBounds(std::vector<Work *> &out, const Rect3s &rect);

	[[nodiscard]] Work *addWorkRaw(std::unique_ptr<Work> &&work);
	[[nodiscard]] bool removeWorkRaw(u32 workId);
};

} // namespace world
