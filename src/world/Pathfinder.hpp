#pragma once

#include <vector>
#include <unordered_map>

#include "content/ItemDef.hpp"
#include "types/types.hpp"

namespace world {

class Pathfinder {
public:
	struct Settings {
		/// Maximum cost of a path
		/// Negative acts as multiplier to aerial distance
		float giveUpCost = -3.f;

		/// If true, the pathfinder will accept the exact end position.
		bool acceptExactTarget = true;

		/// If true, the pathfinder will accept neighbours of the end position.
		bool acceptNeighbours = false;

		/// If true, the pathfinder will accept the level below any otherwise
		/// accepted position.
		bool acceptBelow = false;

		/// Map of content_ids to overridden weight
		std::unordered_map<content_id, float> weights = {};
	};

	virtual ~Pathfinder() = default;

	/// Finds a path.
	///
	/// @param path Output
	/// @param from Start position
	/// @param to End position
	/// @param settings To control pathfinder logic
	/// @return Success value
	[[nodiscard]] virtual bool findPath(std::vector<V3s> &path, const V3s &from,
			const V3s &to, const Settings &settings) = 0;
};

} // namespace world
