#include "Entity.hpp"
#include "World.hpp"
#include <log.hpp>
#include <cassert>
#include <cmath>

using namespace world;

const std::string Entity::PLAYER_TYPE_NAME = "Player";

void Entity::interpolate(float dtime) {
	V3f delta = last_sent_position - position;
	if (delta.sqLength() < 0.001f) {
		setPosition(last_sent_position);
	} else {
		delta *= 1.f - std::pow(0.0000005f, dtime);
		move(delta.x, delta.y);
	}

	f32 deltaYaw = last_sent_yaw - yaw;
	if (deltaYaw > 180.f) {
		deltaYaw = 360.f - deltaYaw;
	}
	if (deltaYaw < -180) {
		deltaYaw += 360.f;
	}

	assert(deltaYaw < 180.1f && deltaYaw > -180.1f);

	setYaw(yaw + deltaYaw * 0.4f);
}

void Entity::setPosition(const V3f &newpos) {
	V3s old_chunk = PosToCPos(position);
	V3s new_chunk = PosToCPos(newpos);

	position = newpos;
	if (old_chunk != new_chunk) {
		world->reinsertEntityToChunk(this);
	}
}

bool Entity::moveTo(const V3f &newpos) {
	if (world->collidesAt(position, newpos)) {
		return false;
	}

	setPosition(newpos);
	return true;
}

bool Entity::jump(const V3f &delta) {
	assert(delta.z == 0);
	V3f up = position + V3f(0, 0, 1);

	// Can jump?
	if (!world->hasGround(position.floor()) || world->hasAnything(up.floor())) {
		return false;
	}

	// Can stand?
	V3f dest = up + delta;
	if (!world->canStandAt(dest.floor())) {
		return false;
	}

	setPosition(dest);
	return true;
}

int Entity::damage(int points) {
	if (points >= hp) {
		Log("Entity", INFO) << "Entity died!";
		int old = hp;
		hp = 0;
		return old;
	} else {
		hp -= points;
		Log("Entity", INFO)
				<< "Entity was damaged by " << points << " hp now " << hp;
		return points;
	}
}

bool Entity::checkForFalling() {
	// Check for falling
	if (!world->hasGround(position.floor())) {
		V3f new_pos = position;
		new_pos.z--;

		if (!world->collidesAt(new_pos, new_pos)) {
			position = new_pos;
			world->reinsertEntityToChunk(this);

			return true;
		}
	}
	return false;
}

std::string Entity::getInfoAsString() const {
	std::stringstream os;

	os << "Entity " << id << " type " << typeName << " at (" << position.x
	   << ", " << position.y << ", " << position.z << ")"
	   << " int-to (" << last_sent_position.x << ", " << last_sent_position.y
	   << ", " << last_sent_position.z << ")";

	return os.str();
}
