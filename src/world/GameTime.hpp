#pragma once

#include <cassert>
#include <cmath>

namespace world {

class GameTime {
public:
	static constexpr float MAX_TIME_OF_DAY = 24;

	float realTimeSinceBeginning = 0;

	/// In hours, 0<=x<24
	/// Eg: 0930 is 9.5
	float timeOfDay = 0;

	/// Seconds per second
	float speed = 24 * 60;

	GameTime() = default;

	explicit GameTime(float timeOfDay) : timeOfDay(timeOfDay) {}

	GameTime(float realTimeSinceBeginning, float timeOfDay, float speed)
			: realTimeSinceBeginning(realTimeSinceBeginning),
			  timeOfDay(timeOfDay), speed(speed) {}

	/// Update the game time based on elapsed real time
	///
	/// @param dtime
	inline void update(float dtime) {
		realTimeSinceBeginning += dtime;

		timeOfDay += speed * dtime / (60.f * 60.f);
		if (timeOfDay >= MAX_TIME_OF_DAY) {
			timeOfDay -= MAX_TIME_OF_DAY;
		}
		assert(timeOfDay <= MAX_TIME_OF_DAY);
	}

	/// Set the time speed from a real-world day length in seconds
	///
	/// @param real-world day length in seconds
	void setSpeedFromDayLength(float daylength) {
		// MAX_TIME = daylength * speed / (60 * 60)
		// 60 * 60 * MAX/dl = speed
		speed = 60.f * 60.f * MAX_TIME_OF_DAY / daylength;
	}

	/// @return Hours, from 0-23
	inline int getHours() const { return (int)std::floor(timeOfDay); }

	/// @return Minutes, from 0-59
	inline int getMinutes() const {
		return (int)std::floor(timeOfDay * 60) % 60;
	}

	/// @return 24-hour clock string
	std::string getString() const {
		char data[6];
		snprintf(&data[0], 6, "%02d:%02d", getHours(), getMinutes());
		return std::string(data, 5);
	}

	/// @return time of day as a fraction of the total day length (0.0 - 1.0)
	inline float asDecimal() const { return timeOfDay / MAX_TIME_OF_DAY; }

	/// @return calculated daylight fraction, from (0.0 - 1.0)
	float getDaylight() const {
		float x = timeOfDay - 12.f;
		float curve = -(x * x) / 36.f + 1.f;
		return std::max(0.f, curve);
	}
};

} // namespace world
