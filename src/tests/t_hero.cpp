#include "tests.hpp"

#include "client/controllers/hero/HeroController.hpp"
#include "mocks.hpp"

#include "DefinitionTestData.hpp"

#include <unordered_map>

using namespace client;
using namespace content;
using namespace world;

namespace {

void initWorld(World &world, content::DefinitionManager &def) {
	WorldChunk *chunk = world.getOrCreateChunk({0, 0, 0});
	for (auto &cid : chunk->terrain) {
		cid = def.getCidFromName("grass");
	}
	chunk->tiles[2 + 4 * CHUNK_SIZE].cid = def.getCidFromName("wall");
	chunk->tiles[3 + 4 * CHUNK_SIZE].cid = def.getCidFromName("wall");
	chunk->tiles[4 + 4 * CHUNK_SIZE].cid = def.getCidFromName("wall");
}

} // namespace

Test(HeroController_ClientEvent_SetInventory) {
	DefinitionManager def;
	defineTestData(&def);

	// Rather confusingly, the ownership of this is passed into setInventory
	auto inv = new Inventory({"aa"});

	MockInput input;
	MockCamera camera;
	EventBus<HeroEvent> heroEvent;
	EventBus<ClientHeroEvent> clientHeroEvent;

	HeroController hero(
			&heroEvent, &clientHeroEvent, &def, nullptr, &input, &camera);

	Assert(hero.getInventory());
	hero.handleClientEvent(ClientHeroEvent::setInventory(inv));
	Assert(hero.getInventory() == inv);
}

Test(HeroController_ClientEvent_SetEntity) {
	DefinitionManager def;
	defineTestData(&def);

	MockInput input;
	MockCamera camera;
	EventBus<HeroEvent> heroEvent;
	EventBus<ClientHeroEvent> clientHeroEvent;

	V3f expectedPos(1.2, 2.3, 4);

	HeroController hero(
			&heroEvent, &clientHeroEvent, &def, nullptr, &input, &camera);

	Entity entity(nullptr, Entity::PLAYER_TYPE_NAME, nullptr);
	entity.setPositionRaw(expectedPos);

	Assert(hero.getPlayerPosition().isInvalid());
	Assert(camera.followEntity == nullptr);

	hero.handleClientEvent(ClientHeroEvent::setEntity(&entity));
	Assert(camera.followEntity == nullptr);

	Assert(!hero.getPlayerPosition().isInvalid());
	Assert(hero.getPlayerPosition() == expectedPos);
}

Test(HeroController_Follows_Entity) {
	DefinitionManager def;
	defineTestData(&def);
	World world{&def};
	initWorld(world, def);

	MockInput input;
	MockCamera camera;
	EventBus<HeroEvent> heroEvent;
	EventBus<ClientHeroEvent> clientHeroEvent;

	HeroController hero(
			&heroEvent, &clientHeroEvent, &def, &world, &input, &camera);

	auto entity =
			world.createEntity(V3f(1.2, 2.3, 4), Entity::PLAYER_TYPE_NAME);

	Assert(camera.followEntity == nullptr);

	hero.handleClientEvent(ClientHeroEvent::setEntity(entity));
	Assert(camera.followEntity == nullptr);

	hero.update_if_enabled(0.1f);
	Assert(camera.followEntity == entity);
}
