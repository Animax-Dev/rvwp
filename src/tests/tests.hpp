#pragma once

#include <string>
#include <log.hpp>
#include <functional>
#include <map>
#include <utility>

#define Test(name)                                                             \
	void test_##name();                                                        \
	static bool test_##name##_registered =                                     \
			TestRunner::Register(#name, &test_##name);                         \
	void test_##name()

class AssertionFailedException : public std::exception {
public:
	std::string msg;

	explicit AssertionFailedException(std::string msg) : msg(std::move(msg)) {}

	const char *what() const noexcept override { return msg.c_str(); }
};

class TestRunner {
public:
	static bool Register(
			const std::string &name, std::function<void()> func) noexcept;
	static bool RunTests();

private:
	static std::map<std::string, std::function<void()>> Tests;
};

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

#define Assert(x)                                                              \
	if (!(x)) {                                                                \
		throw AssertionFailedException(                                        \
				"Test " #x " failed at " __FILE__ ":" STR(__LINE__));          \
	}
