#include "tests.hpp"
#include <sstream>
#include "../world/Plot.hpp"

using namespace world;

Test(PlotEmpty) {
	Plot plot(1, "hello", {});
	Assert(plot.bounds.empty());
	Assert(plot.peekBoxes().empty());

	Rect3s rect1 = {{1, 1, 1}, {5, 5, 5}};
	plot.addBox(rect1);
	Assert(plot.peekBoxes().size() == 1);
	Assert(plot.peekBoxes()[0] == rect1);
}

Test(PlotContainsTests) {
	Rect3s rect1 = {{1, 1, 1}, {5, 5, 5}};
	Rect3s rect2 = {{5, 5, 5}, {10, 10, 10}};
	Plot plot(1, "hello", rect1);

	Assert(plot.peekBoxes().size() == 1);
	Assert(plot.contains({1, 1, 1}));
	Assert(plot.contains({3, 3, 3}));
	Assert(plot.contains({5, 5, 5}));
	Assert(!plot.contains({5, 5, 6}));

	plot.addBox(rect2);

	Assert(!plot.contains({-3, 2, 1}));
	Assert(plot.contains({1, 1, 1}));
	Assert(plot.contains({3, 3, 3}));
	Assert(plot.contains({5, 5, 5}));
	Assert(plot.contains({5, 5, 6}));
	Assert(plot.contains({14, 12, 11}));
	Assert(!plot.contains({1, 13, 16}));
	Assert(!plot.contains({1, 6, 1}));
	Assert(!plot.contains({12, 13, 16}));
	Assert(!plot.contains({20, 20, 20}));

	Assert(plot.peekBoxes().size() == 2);
	Assert(plot.peekBoxes()[0] == rect1);
	Assert(plot.peekBoxes()[1] == rect2);

	plot.clearBoxes();
	Assert(plot.bounds.empty());
	Assert(plot.peekBoxes().empty());

	Assert(!plot.contains({-3, 2, 1}));
	Assert(!plot.contains({1, 1, 1}));
	Assert(!plot.contains({3, 3, 3}));
	Assert(!plot.contains({5, 5, 5}));
	Assert(!plot.contains({5, 5, 6}));
	Assert(!plot.contains({14, 12, 11}));
	Assert(!plot.contains({12, 13, 16}));
	Assert(!plot.contains({20, 20, 20}));

	plot.addBoxes({rect1, rect2});

	Assert(!plot.contains({-3, 2, 1}));
	Assert(plot.contains({1, 1, 1}));
	Assert(plot.contains({3, 3, 3}));
	Assert(plot.contains({5, 5, 5}));
	Assert(plot.contains({5, 5, 6}));
	Assert(plot.contains({14, 12, 11}));
	Assert(!plot.contains({12, 13, 16}));
	Assert(!plot.contains({20, 20, 20}));
}

Test(PlotWorkTest) {
	Rect3s plotRect = {{1, 1, 1}, {5, 5, 5}};
	Plot plot(1, "hello", plotRect);

	Rect3s rect1 = {{2, 2, 1}, {1, 1, 1}};
	Rect3s rect2 = {{0, 1, 1}, {1, 1, 1}};
	Rect3s rect3 = {{5, 1, 1}, {1, 1, 1}};
	Rect3s rect4 = {{6, 1, 1}, {1, 1, 1}};

	std::vector<Work *> works;
	plot.getWorkInBounds(works, plotRect);
	Assert(works.empty());

	Assert(plot.addWorkRaw(
			std::make_unique<Work>("build", rect1, content::Material())));
	Assert(!plot.addWorkRaw(
			std::make_unique<Work>("build", rect1, content::Material())));
	Assert(!plot.addWorkRaw(
			std::make_unique<Work>("build", rect2, content::Material())));
	Assert(plot.addWorkRaw(
			std::make_unique<Work>("build", rect3, content::Material())));
	Assert(!plot.addWorkRaw(
			std::make_unique<Work>("build", rect3, content::Material())));
	Assert(!plot.addWorkRaw(
			std::make_unique<Work>("build", rect4, content::Material())));
}
