#pragma once

#include <client/input/IInput.hpp>
#include <client/controllers/ICamera.hpp>

using namespace client;

class MockInput : public IInput {
public:
	std::map<int, std::function<void()>> listeners;
	V3f moveVector{};
	std::optional<V3f> cameraPosition{};

	V3f getMoveVector() override { return moveVector; }

	std::optional<V3f> getCursorPosition(const V3f &origin) override {
		return cameraPosition;
	}

	bool isVirtualCursor() const override { return false; }

	InputConnection connect(
			Action action, const std::function<void()> &listener) override {
		listeners[(int)action] = listener;
		return InputConnection();
	}

	bool isActive(Action action) override { return false; }

	void runListener(Action action) { listeners[(int)action](); }

	ActionState getCurrentActionState() const override { return INGAME; }

	void setActionStateProvider(
			const std::function<ActionState()> &v) override {}
};

class MockCamera : public ICamera {
public:
	Camera camera{};
	world::Entity *followEntity = nullptr;
	sf::Vector2f moveDelta{};

	void follow(world::Entity *e) override { followEntity = e; }

	void move(sf::Vector2f delta) override { moveDelta += delta; }

	const Camera &getCamera() const override { return camera; }
};
