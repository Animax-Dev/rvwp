#include "tests.hpp"
#include <sstream>
#include "../world/World.hpp"
#include "../world/Raycast.hpp"

#include "DefinitionTestData.hpp"

using namespace world;

Test(ChunkInsertGetTest) {
	content::DefinitionManager defman{};
	auto world = std::make_unique<World>(&defman);

	{
		Assert(world->getChunkCount() == 0);
		WorldChunk *a = world->createChunkAndActivate(V3s(0, 0, 0));
		Assert(world->getChunk(V3s(0, 0, 0)) == a);
		Assert(world->getChunkCount() == 1);
	}
	{
		WorldChunk *b = world->createChunkAndActivate(V3s(0, 0, 1));
		Assert(world->getChunk(V3s(0, 0, 1)) == b);
		Assert(world->getChunkCount() == 2);
	}
	{
		WorldChunk *c = world->createChunkAndActivate(V3s(1, 1, 1));
		Assert(world->getChunk(V3s(1, 1, 1)) == c);
		Assert(world->getChunkCount() == 3);
	}
	{
		WorldChunk *d = world->createChunkAndActivate(V3s(0, 1, 0));
		Assert(world->getChunk(V3s(0, 1, 0)) == d);
		Assert(world->getChunkCount() == 4);

		Assert(world->getOrCreateChunk({0, 1, 0}) == d);
		Assert(world->getChunkCount() == 4);
	}
	{
		auto d = world->createChunkAndActivate({0, 1, 1});
		Assert(d && world->getChunk(V3s(0, 1, 1)) == d);
		Assert(world->getChunkCount() == 5);
	}
}

Test(ChunkAreaGetTest) {

	content::DefinitionManager defman{};
	auto world = std::make_unique<World>(&defman);
	Assert(world);

	WorldChunk *a = world->createChunkAndActivate(V3s(0, 0, 0));
	world->createChunkAndActivate(V3s(0, 0, 1));
	world->createChunkAndActivate(V3s(1, 1, 1));
	WorldChunk *d = world->createChunkAndActivate(V3s(0, 1, 0));

	std::vector<WorldChunk *> res;
	world->getChunksInArea(res, V3f(0, 0, 0), CHUNK_SIZE);

	Assert(res.size() == 2);
	Assert(res[0] != res[1]);

	for (auto ch : res) {
		Assert(ch == a || ch == d);
	}
}

Test(ChunkNeighbourTests) {
	content::DefinitionManager def;
	World world{&def};

	for (int z = -2; z <= 2; z++) {
		for (int y = -2; y <= 2; y++) {
			for (int x = -2; x <= 2; x++) {
				world.createChunkAndActivate(V3s(x, y, z));
			}
		}
	}

	std::vector<WorldChunk *> chunks;
	chunks.reserve(3 * 3 * 3 - 1);
	world.getNeighbouringChunks3d(chunks, V3s(0, 0, 0));
	Assert(chunks.size() == 3 * 3 * 3 - 1);

	for (const auto &chunk : chunks) {
		Assert(chunk->pos.x >= -1 && chunk->pos.x <= 1);
		Assert(chunk->pos.y >= -1 && chunk->pos.y <= 1);
		Assert(chunk->pos.z >= -1 && chunk->pos.z <= 1);
	}
}

Test(ChunkDirtyTests) {
	content::DefinitionManager defman{};
	World world{&defman};

	std::vector<WorldChunk *> results;

	world.getDirtyChunks(results);
	Assert(results.empty());

	auto chunk = world.createChunkAndActivate({0, 0, 0});

	world.getDirtyChunks(results);
	Assert(results.empty());

	chunk->dirty = true;

	world.getDirtyChunks(results);
	Assert(results.size() == 1);
	Assert(results[0] == chunk);
}

Test(RaycastTileTests) {
	content::DefinitionManager defman{};
	World world{&defman};

	world.createChunkAndActivate(V3s(0, 0, 0));
	world.createChunkAndActivate(V3s(1, 0, 0));
	world.createChunkAndActivate(V3s(0, 1, 0));

	Raycast tracer(&world);

	SelectionSpec spec = tracer.trace(V3f(0, 0, 0), V3f(0, 1, 0), 100);
	Assert(spec.type == SelectionSpec::SST_None);

	spec = tracer.trace_yaw(V3f(0, 0, 0), 0, 100);
	Assert(spec.type == SelectionSpec::SST_None);

	Assert(world.set(V3s(5, 0, 0), ChunkLayer::Tile, 1));

	spec = tracer.trace_yaw(V3f(0, 0, 0), 0, 100);
	Assert(spec.type == SelectionSpec::SST_Tile);
	Assert(spec.tile == world.get(V3s(5, 0, 0), ChunkLayer::Tile));

	spec = tracer.trace_yaw(V3f(0, 0, 0), 3.1415f / 2.0f, 100);
	Assert(spec.type == SelectionSpec::SST_None);

	Assert(world.set(V3s(0, 5, 0), ChunkLayer::Tile, 1));

	spec = tracer.trace(V3f(0, 0, 0), V3f(0, 1, 0), 100);
	Assert(spec.type == SelectionSpec::SST_Tile);
	Assert(spec.tile == world.get(V3s(0, 5, 0), ChunkLayer::Tile));

	spec = tracer.trace_yaw(V3f(0, 0, 0), 3.1415f / 2.0f, 100);
	Assert(spec.type == SelectionSpec::SST_Tile);
	Assert(spec.tile == world.get(V3s(0, 5, 0), ChunkLayer::Tile));

	auto e2 = world.createEntity(V3f(0, 3, 0), Entity::PLAYER_TYPE_NAME);
	Assert(e2->id == 1);
	Assert(world.getEntityById(e2->id) == e2);
	Assert(e2->parent);
	Assert(e2->parent->entities.size() == 1);

	spec = tracer.trace_yaw(V3f(0, 0, 0), 3.1415f / 2.0f, 100);
	Assert(spec.type == SelectionSpec::SST_Entity);
	Assert(spec.entity == e2);

	spec = tracer.trace_to(V3f(0, 0, 0), V3f(0, 10, 0));
	Assert(spec.type == SelectionSpec::SST_Entity);
	Assert(spec.entity == e2);

	spec = tracer.trace_to(V3f(0, 0, 0), V3f(4.9, 0, 0));
	Assert(spec.type == SelectionSpec::SST_None);

	spec = tracer.trace_to(V3f(0, 0, 0), V3f(0, 0, 0));
	Assert(spec.type == SelectionSpec::SST_None);
}

Test(TileSetGetTests) {
	content::DefinitionManager defman{};
	World world{&defman};

	Assert(!world.set(V3s(0, 1, 0), ChunkLayer::Floor, 1));

	WorldChunk *chunk = world.createChunkAndActivate(V3s(0, 0, 0));

	Assert(!chunk->floort[0 + 1 * CHUNK_SIZE]);
	Assert(!world.get(V3s(0, 1, 0), ChunkLayer::Floor));
	Assert(world.set(V3s(0, 1, 0), ChunkLayer::Floor, 1));
	Assert(chunk->floort[0 + 1 * CHUNK_SIZE].cid == 1);
	Assert(world.get(V3s(0, 1, 0), ChunkLayer::Floor)->cid == 1);
	Assert(chunk->floort[0 + 1 * CHUNK_SIZE]);
	Assert(world.get(V3s(0, 1, 0), ChunkLayer::Floor));

	const WorldTile tile{4};
	Assert(world.get(V3s(0, 1, 0), ChunkLayer::Tile) == nullptr);
	Assert(world.set({0, 1, 0}, ChunkLayer::Tile, tile));
	Assert(world.get(V3s(0, 1, 0), ChunkLayer::Tile)->cid == tile.cid);
	Assert(chunk->floort[0 + 1 * CHUNK_SIZE].cid == 1);

	const WorldTile tile2{2};
	Assert(world.get(V3s(0, 1, 0), ChunkLayer::Tile)->cid == tile.cid);
	Assert(world.get(V3s(0, 1, 0), ChunkLayer::Floor)->cid == 1);
	Assert(world.set({0, 1, 0}, ChunkLayer::Floor, tile2));
	Assert(world.get(V3s(0, 1, 0), ChunkLayer::Floor)->cid == 2);
	Assert(chunk->floort[0 + 1 * CHUNK_SIZE].cid == 2);
}

Test(CollidesAtTests) {
	content::DefinitionManager defman{};
	World world{&defman};

	Assert(world.collidesAt({5, 5, 5}, {5, 5, 5}));
	auto c = world.createChunkAndActivate({0, 0, 5});
	world.activateChunk(c);

	Assert(!world.collidesAt({5, 5, 5}, {5, 5, 5}));

	Assert(world.set({5, 5, 5}, ChunkLayer::Tile, 2));

	Assert(world.collidesAt({5, 5, 5}, {5, 5, 5}));
}

Test(GroundAndSolid) {
	content::DefinitionManager defman{};
	World world{&defman};

	Assert(world.hasGround({5, 5, 5}));
	Assert(world.hasAnything({5, 5, 5}));
	Assert(!world.canStandAt({5, 5, 5}));
	auto c = world.createChunkAndActivate({0, 0, 5});
	world.activateChunk(c);
	Assert(!world.hasGround({5, 5, 5}));
	Assert(!world.hasAnything({5, 5, 5}));
	Assert(!world.canStandAt({5, 5, 5}));

	Assert(world.set({5, 5, 5}, ChunkLayer::Tile, 2));

	Assert(!world.hasGround({5, 5, 5}));
	Assert(world.hasAnything({5, 5, 5}));
	Assert(!world.canStandAt({5, 5, 5}));

	c->terrain[5 + 5 * 16] = 8;

	Assert(world.hasGround({5, 5, 5}));
	Assert(world.hasAnything({5, 5, 5}));
	Assert(!world.canStandAt({5, 5, 5}));

	Assert(!world.hasGround({6, 5, 5}));
	Assert(!world.hasAnything({6, 5, 5}));
	Assert(!world.canStandAt({6, 5, 5}));

	c->terrain[6 + 5 * 16] = 8;

	Assert(world.hasGround({6, 5, 5}));
	Assert(world.hasAnything({6, 5, 5}));
	Assert(world.canStandAt({6, 5, 5}));
}

Test(WorldMetaQuery) {
	content::DefinitionManager definitionManager{};
	defineTestData(&definitionManager);

	World world{&definitionManager};

	const V3s metaTilePos{2, 2, 0};
	const V3f centerPos{5, 5, 0};
	const auto cid = definitionManager.getCidFromName("wall");

	// Pre-check
	std::vector<std::pair<V3s, WorldTile *>> out;
	world.findNodesWithMetaData(
			out, centerPos, 3.f, ChunkLayer::Tile, "work_provider");
	Assert(out.empty());

	// Set chunk and tile
	world.activateChunk(world.createChunkAndActivate({0, 0, 0}));
	Assert(world.set(metaTilePos, ChunkLayer::Tile, cid));
	Assert(world.get(metaTilePos, ChunkLayer::Tile, nullptr)->cid == cid);

	// Check
	world.findNodesWithMetaData(
			out, centerPos, 3.f, ChunkLayer::Tile, "work_provider");
	Assert(out.empty());

	// Set metadata without key
	auto meta = world.getMetaData(metaTilePos, ChunkLayer::Tile);
	Assert(meta.has_value());
	meta.value()->set<std::string>("foo", "bar");

	// Check
	world.findNodesWithMetaData(
			out, centerPos, 3.f, ChunkLayer::Tile, "work_provider");
	Assert(out.empty());

	// Set checked key
	meta.value()->set<std::string>("work_provider", "bar");

	// Check it works
	world.findNodesWithMetaData(
			out, centerPos, 3.f, ChunkLayer::Tile, "work_provider");
	Assert(out.size() == 1);
	Assert(out[0].first == metaTilePos);
	Assert(out[0].second->cid == cid);

	// Check layers
	out.clear();
	world.findNodesWithMetaData(
			out, centerPos, 3.f, ChunkLayer::Floor, "work_provider");
	Assert(out.empty());
}

Test(PlotTests) {
	content::DefinitionManager defman{};
	World world{&defman};

	Assert(world.getPlot({5, 5, 0}) == nullptr);
	Assert(world.getPlot({5, 5, 5}) == nullptr);

	auto home = world.createPlot("Home", {{3, 3, 3}, {6, 6, 6}});

	Assert(home);
	Assert(home->name == "Home");
	Assert(world.getPlot({5, 5, 0}) == nullptr);
	Assert(world.getPlot({5, 5, 5}) == home);
}

Test(WorldTileTimerTest) {
	content::DefinitionManager defman{};
	defineTestData(&defman);
	World world{&defman};

	const TileTimer expectedTimer1 = {
			V3s(4, 4, 0), ChunkLayer::Tile, "foo", 90.f};

	const TileTimer expectedTimer2 = {
			V3s(6, 4, 0), ChunkLayer::Tile, "bar", 50.f};

	const TileTimer expectedTimer3 = {
			V3s(18, 7, 0), ChunkLayer::Tile, "baz", 100.f};

	WorldChunk *chunk1 = world.getOrCreateChunk({0, 0, 0});
	WorldChunk *chunk2 = world.getOrCreateChunk({1, 0, 0});

	std::vector<TileTimer> timers;
	Assert(chunk1->timers.empty());
	Assert(chunk2->timers.empty());
	world.popExpiredTimers(timers, 100.f);
	Assert(timers.empty());
	Assert(chunk1->timers.empty());
	Assert(chunk2->timers.empty());

	Assert(world.pushTimer(expectedTimer1));
	Assert(chunk1->timers.size() == 1);
	Assert(chunk2->timers.empty());

	world.popExpiredTimers(timers, 100.f);
	Assert(timers.empty());
	Assert(chunk1->timers.size() == 1);
	Assert(chunk2->timers.empty());

	world.activateChunk(chunk1);
	world.activateChunk(chunk2);

	world.popExpiredTimers(timers, 50.f);
	Assert(timers.empty());
	Assert(chunk1->timers.size() == 1);
	Assert(chunk2->timers.empty());

	world.popExpiredTimers(timers, 89.f);
	Assert(timers.empty());
	Assert(chunk1->timers.size() == 1);
	Assert(chunk2->timers.empty());

	world.popExpiredTimers(timers, 100.f);
	Assert(timers.size() == 1);
	Assert(timers[0].position == expectedTimer1.position);
	Assert(timers[0].layer == expectedTimer1.layer);
	Assert(timers[0].event == expectedTimer1.event);
	Assert(chunk1->timers.empty());
	Assert(chunk2->timers.empty());
	timers.clear();

	Assert(world.pushTimer(expectedTimer1));
	Assert(world.pushTimer(expectedTimer2));
	Assert(world.pushTimer(expectedTimer3));

	Assert(chunk1->timers.size() == 2);
	Assert(chunk2->timers.size() == 1);
	world.popExpiredTimers(timers, 49.f);
	Assert(timers.empty());
	Assert(chunk1->timers.size() == 2);
	Assert(chunk2->timers.size() == 1);
	world.popExpiredTimers(timers, 55.f);
	Assert(timers.size() == 1);
	Assert(timers[0].position == expectedTimer2.position);
	Assert(timers[0].layer == expectedTimer2.layer);
	Assert(timers[0].event == expectedTimer2.event);
	Assert(chunk1->timers.size() == 1);
	Assert(chunk2->timers.size() == 1);
	Assert(chunk1->timers[0].layer == expectedTimer1.layer);
	Assert(chunk1->timers[0].event == expectedTimer1.event);
	Assert(chunk2->timers[0].layer == expectedTimer3.layer);
	Assert(chunk2->timers[0].event == expectedTimer3.event);
	timers.clear();

	world.popExpiredTimers(timers, 200.f);
	Assert(timers.size() == 2);
	Assert(timers[1].position == expectedTimer1.position);
	Assert(timers[1].layer == expectedTimer1.layer);
	Assert(timers[1].event == expectedTimer1.event);
	Assert(timers[0].position == expectedTimer3.position);
	Assert(timers[0].layer == expectedTimer3.layer);
	Assert(timers[0].event == expectedTimer3.event);
	Assert(chunk1->timers.empty());
	Assert(chunk2->timers.empty());
}
