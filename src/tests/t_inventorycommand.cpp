#include "tests.hpp"
#include "content/InventoryCommand.hpp"

using namespace content;

namespace {

const std::string A = "wall";
const std::string B = "sword";

void initInventory(content::Inventory &inventory) {
	inventory.newList("main", 6, 4);
	inventory.newList("hotbar", 2, 1);
	inventory.set("hotbar", 0, ItemStack(A, 6));
	inventory.set("main", 5, ItemStack(B, 10));
}
} // namespace

Test(InventoryCommand_Swap_With_Empty) {
	auto inventoryLocation = InventoryLocation::FromPlayer("test");
	Inventory inventory(inventoryLocation);
	initInventory(inventory);

	InventoryProvider provider = {[&](auto loc) -> content::Inventory * {
		if (loc == inventoryLocation.name) {
			return &inventory;
		} else {
			return nullptr;
		}
	}};

	InventoryCommand cmd(InventoryCommand::SWAP,
			{inventoryLocation, "hotbar", 0}, {inventoryLocation, "main", 1},
			6);

	Assert(inventory.get("hotbar", 0).name == A);
	Assert(inventory.get("hotbar", 0).count == 6);
	Assert(inventory.get("main", 1).empty());
	Assert(inventory.get("main", 0).empty());
	Assert(cmd.perform(provider));
	Assert(inventory.get("main", 0).empty());
	Assert(inventory.get("hotbar", 0).empty());
	Assert(inventory.get("main", 1).name == A);
	Assert(inventory.get("main", 1).count == 6);
}

Test(InventoryCommand_Swap_With_Other) {
	auto inventoryLocation = InventoryLocation::FromPlayer("test");
	Inventory inventory(inventoryLocation);
	initInventory(inventory);

	InventoryProvider provider = {[&](auto loc) -> content::Inventory * {
		if (loc == inventoryLocation.name) {
			return &inventory;
		} else {
			return nullptr;
		}
	}};

	InventoryCommand cmd(InventoryCommand::SWAP,
			{inventoryLocation, "hotbar", 0}, {inventoryLocation, "main", 5},
			6);

	Assert(inventory.get("hotbar", 0).name == A);
	Assert(inventory.get("hotbar", 0).count == 6);
	Assert(inventory.get("main", 5).name == B);
	Assert(inventory.get("main", 5).count == 10);
	Assert(inventory.get("main", 0).empty());
	Assert(cmd.perform(provider));
	Assert(inventory.get("main", 0).empty());
	Assert(inventory.get("hotbar", 0).name == B);
	Assert(inventory.get("hotbar", 0).count == 10);
	Assert(inventory.get("main", 5).name == A);
	Assert(inventory.get("main", 5).count == 6);
}

Test(InventoryCommand_Move_With_Empty) {
	auto inventoryLocation = InventoryLocation::FromPlayer("test");
	Inventory inventory(inventoryLocation);
	initInventory(inventory);

	InventoryProvider provider = {[&](auto loc) -> content::Inventory * {
		if (loc == inventoryLocation.name) {
			return &inventory;
		} else {
			return nullptr;
		}
	}};

	InventoryCommand cmd(InventoryCommand::MOVE,
			{inventoryLocation, "hotbar", 0}, {inventoryLocation, "main", 1},
			6);

	Assert(inventory.get("hotbar", 0).name == A);
	Assert(inventory.get("hotbar", 0).count == 6);
	Assert(inventory.get("main", 0).empty());
	Assert(inventory.get("main", 1).empty());
	Assert(cmd.perform(provider));
	Assert(inventory.get("hotbar", 0).empty());
	Assert(inventory.get("main", 0).empty());
	Assert(inventory.get("main", 1).name == A);
	Assert(inventory.get("main", 1).count == 6);
}

Test(InventoryCommand_Move_Partial_With_Empty) {
	auto inventoryLocation = InventoryLocation::FromPlayer("test");
	Inventory inventory(inventoryLocation);
	initInventory(inventory);

	InventoryProvider provider = {[&](auto loc) -> content::Inventory * {
		if (loc == inventoryLocation.name) {
			return &inventory;
		} else {
			return nullptr;
		}
	}};

	InventoryCommand cmd(InventoryCommand::MOVE,
			{inventoryLocation, "hotbar", 0}, {inventoryLocation, "main", 1},
			2);

	Assert(inventory.get("hotbar", 0).name == A);
	Assert(inventory.get("hotbar", 0).count == 6);
	Assert(inventory.get("main", 1).empty());
	Assert(inventory.get("main", 0).empty());
	Assert(cmd.perform(provider));
	Assert(inventory.get("main", 0).empty());
	Assert(inventory.get("hotbar", 0).name == A);
	Assert(inventory.get("hotbar", 0).count == 4);
	Assert(inventory.get("main", 1).name == A);
	Assert(inventory.get("main", 1).count == 2);
}
