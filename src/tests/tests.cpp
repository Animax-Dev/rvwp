#include <utility>

#include <map>
#include "tests.hpp"

#define COLOR_CLEAR "\033[0m"
#define COLOR_RED "\033[;31m"
#define COLOR_GREEN "\033[;32m"

bool do_test(const std::string &name, const std::function<void()> &test) {
	bool eval = true;
	try {
		test();
	} catch (const AssertionFailedException &ex) {
		VLOG_F(loguru::Verbosity_ERROR, "AssertionFailedException: %s",
				ex.what());
		eval = false;
	}

	VLOG_F((eval ? loguru::Verbosity_INFO : loguru::Verbosity_ERROR),
			"[Tests] %s%-30s :: %s%s", (eval ? COLOR_GREEN : COLOR_RED),
			name.c_str(), (eval ? "PASSED" : "FAILED"), COLOR_CLEAR);

	return eval;
}

std::map<std::string, std::function<void()>> TestRunner::Tests;

bool TestRunner::Register(
		const std::string &name, std::function<void()> func) noexcept {
	auto it = Tests.find(name);
	if (it == Tests.end()) {
		Tests[name] = std::move(func);
		return true;
	}
	return false;
}

bool TestRunner::RunTests() {
	bool eval = true;
	for (const auto &method : Tests) {
		eval = do_test(method.first, method.second) && eval;
	}

	return eval;
}
