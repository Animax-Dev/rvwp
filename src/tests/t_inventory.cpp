#include "tests.hpp"
#include "content/Inventory.hpp"

using namespace content;

Test(InventoryTests) {
	Inventory inv{{"player:one"}};

	Assert(inv.size() == 0);
	inv.newList("main", 3, 3);
	Assert(inv.size() == 1);
	InventoryList *list = inv.get("main").value();
	Assert(list->items.size() == 9);
	ItemStack stack, ret;

	const std::string expectedName1 = "wall";

	stack.name = expectedName1;
	stack.count = 150;
	ret = list->addItem(stack);
	Assert(ret.name == expectedName1 && ret.count == 0);
	Assert(list->items[0].name == expectedName1 && list->items[0].count == 100);
	Assert(list->items[1].name == expectedName1 && list->items[1].count == 50);
	Assert(list->items[2].name.empty() && list->items[2].count == 0);

	const std::string expectedName2 = "sword";

	stack.name = expectedName2;
	stack.count = 50;
	ret = list->addItem(stack);
	Assert(ret.name == expectedName2 && ret.count == 0);
	Assert(list->items[0].name == expectedName1 && list->items[0].count == 100);
	Assert(list->items[1].name == expectedName1 && list->items[1].count == 50);
	Assert(list->items[2].name == expectedName2 && list->items[2].count == 50);
	Assert(list->items[3].name.empty() && list->items[3].count == 0);

	stack.name = expectedName2;
	stack.count = 700;
	ret = list->addItem(stack);
	Assert(ret.name == expectedName2 && ret.count == 50);
	Assert(list->items[0].name == expectedName1 && list->items[0].count == 100);
	Assert(list->items[1].name == expectedName1 && list->items[1].count == 50);
	for (int i = 2; i < 9; ++i)
		Assert(list->items[i].name == expectedName2 &&
				list->items[i].count == 100);

	ItemStack &stack2 = list->get(0);
	stack2.count--;
	Assert(stack2.count == 99);
	Assert(list->items[0].name == expectedName1 && list->items[0].count == 99);
	Assert(list->items[1].name == expectedName1 && list->items[1].count == 50);
	for (int i = 2; i < 9; ++i) {
		Assert(list->items[i].name == expectedName2 &&
				list->items[i].count == 100);
	}
	stack2.count++;
	Assert(list->items[0].name == expectedName1 && list->items[0].count == 100);

	stack.name = expectedName2;
	stack.count = 50;
	ret = list->takeItem(stack);
	Assert(ret.name == expectedName2 && ret.count == 50);
	Assert(list->items[0].name == expectedName1 && list->items[0].count == 100);
	Assert(list->items[1].name == expectedName1 && list->items[1].count == 50);
	Assert(list->items[2].name == expectedName2 && list->items[2].count == 50);
	for (int i = 3; i < 9; ++i)
		Assert(list->items[i].name == expectedName2 &&
				list->items[i].count == 100);

	stack.name = expectedName2;
	stack.count = 50;
	ret = list->takeItem(stack);
	Assert(ret.name == expectedName2 && ret.count == 50);
	Assert(list->items[0].name == expectedName1 && list->items[0].count == 100);
	Assert(list->items[1].name == expectedName1 && list->items[1].count == 50);
	Assert(list->items[2].name.empty() && list->items[2].count == 0);
	for (int i = 3; i < 9; ++i)
		Assert(list->items[i].name == expectedName2 &&
				list->items[i].count == 100);

	stack.name = expectedName1;
	stack.count = 200;
	ret = list->takeItem(stack);
	Assert(ret.name == expectedName1 && ret.count == 150);
	Assert(list->items[0].name.empty() && list->items[0].count == 0);
	Assert(list->items[1].name.empty() && list->items[1].count == 0);
	Assert(list->items[2].name.empty() && list->items[2].count == 0);
	for (int i = 3; i < 9; ++i)
		Assert(list->items[i].name == expectedName2 &&
				list->items[i].count == 100);
}
