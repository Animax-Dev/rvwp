#include "tests.hpp"

#include <SFML/Graphics.hpp>
#include "../types/Rect.hpp"

using namespace types;

Test(RectCreateTests) {
	{
		Rect3s rect(V3s(0, 0, 0), V3s(5, 5, 5));
		Assert(rect.minPos == V3s(0, 0, 0));
		Assert(rect.size == V3s(5, 5, 5));
	}

	{
		sf::IntRect sfrect(13, 14, 1, 2);
		Rect3s rect(sfrect, 15, 3);
		Assert(rect.minPos == V3s(13, 14, 15));
		Assert(rect.size == V3s(1, 2, 3));
		Assert((sf::IntRect)rect == sfrect);
	}

	{
		Rect3s rect;
		Assert(rect.minPos == V3s(0, 0, 0));
		Assert(rect.size == V3s(0, 0, 0));
	}
}

Test(RectContainsVectorTests) {
	Rect3s rect(V3s(0, 0, 0), V3s(6, 6, 6));
	Assert(rect.contains(V3s(0, 0, 0)));
	Assert(rect.contains(V3s(2, 5, 2)));
	Assert(rect.contains(V3s(5, 5, 5)));
	Assert(!rect.contains(V3s(5, 5, 6)));
	Assert(!rect.contains(V3s(5, 6, 5)));
	Assert(!rect.contains(V3s(6, 5, 5)));
	Assert(!rect.contains(V3s(-1, 5, 5)));
	Assert(!rect.contains(V3s(-1, 3, 3)));
}

Test(RectIntersectsTests) {
	Rect3s rect(V3s(0, 0, 0), V3s(6, 6, 6));
	Assert(rect.intersects({{3, 3, 3}, {3, 3, 3}}));
	Assert(!rect.intersects({{6, 6, 6}, {3, 3, 3}}));
	Assert(rect.intersects({{1, 1, 1}, {1, 1, 1}}));
	Assert(rect.intersects({{-3, -3, -3}, {6, 6, 6}}));
}

Test(RectContainsRectTests) {
	Rect3s rect(V3s(0, 0, 0), V3s(6, 6, 6));
	Assert(rect.contains({{3, 3, 3}, {3, 3, 3}}));
	Assert(rect.contains({{5, 5, 5}, {1, 1, 1}}));
	Assert(rect.contains({{5, 5, 5}, {1, 0, 0}}));
	Assert(rect.contains({{4, 4, 4}, {2, 2, 2}}));
	Assert(!rect.contains({{5, 5, 5}, {2, 2, 2}}));
	Assert(!rect.contains({{5, 5, 5}, {3, 3, 3}}));
	Assert(rect.contains({{1, 1, 1}, {1, 1, 1}}));
	Assert(!rect.contains({{-3, -3, -3}, {6, 6, 6}}));

	Rect3s rect2(V3s(0, 0, -1), V3s(6, 6, 1));
	Assert(rect2.contains({{3, 3, -1}, {1, 1, 1}}));
	Assert(!rect2.contains({{3, 3, 0}, {1, 1, 1}}));
}

Test(RectExtendTests) {
	{
		Rect3s rect1({0, 0, 0}, {3, 3, 3});
		rect1.extend(rect1);

		Assert(rect1.minPos == V3s(0, 0, 0));
		Assert(rect1.size == V3s(3, 3, 3));
	}

	{
		Rect3s rect1({0, 0, 0}, {3, 3, 3});
		const Rect3s rect2({3, 3, 3}, {3, 3, 3});
		rect1.extend(rect2);

		Assert(rect1.minPos == V3s(0, 0, 0));
		Assert(rect1.size == V3s(6, 6, 6));
	}

	{
		Rect3s rect1({0, 0, 0}, {3, 3, 3});
		const Rect3s rect2({6, 6, 6}, {3, 3, 3});
		rect1.extend(rect2);

		Assert(rect1.minPos == V3s(0, 0, 0));
		Assert(rect1.size == V3s(9, 9, 9));
	}

	{
		Rect3s rect1({0, 0, 0}, {3, 3, 3});
		const Rect3s rect2({-3, -3, -3}, {3, 3, 3});
		rect1.extend(rect2);

		Assert(rect1.minPos == V3s(-3, -3, -3));
		Assert(rect1.size == V3s(6, 6, 6));
	}

	{
		Rect3s rect1({3, 3, 3}, {0, 0, 0});
		const Rect3s rect2({-3, -3, -3}, {3, 3, 3});
		rect1.extend(rect2);

		Assert(rect1 == rect2);
	}
}
