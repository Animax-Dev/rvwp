#include "ModFinder.hpp"

#include <filesystem>

namespace fs = std::filesystem;
using namespace scripting;

void ModFinder::addModsInPath(const std::string &dir) {
	for (const auto &entry : fs::directory_iterator(dir)) {
		if (!entry.is_directory()) {
			continue;
		}

		std::string path = entry.path().string();
		if (fs::exists(path + "/mod.json")) {
			mods.push_back(ModConfig::readFromFile(path + "/mod.json", path));
		} else {
			addModsInPath(path);
		}
	}
}
