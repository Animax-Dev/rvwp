#include "LuaContent.hpp"

using namespace scripting;

content::Material scripting::LuaToMaterial(const sol::object &obj) {
	using namespace std::string_literals;

	if (obj.is<std::string>()) {
		return content::Material(obj.as<std::string>());
	}

	sol::table table = obj.as<sol::table>();

	std::string sType = table.get_or("type", "static"s);
	auto optionalType = content::strToMaterialType(sType);
	if (!optionalType.has_value()) {
		throw sol::error("Unknown material type " + sType);
	}

	content::Material material;
	material.type = optionalType.value();
	material.name = table.get_or("texture", "tileset.diffuse.png"s);
	material.x = table.get_or("x", 0);
	material.y = table.get_or("y", 0);
	material.w = table.get_or("w", TILE_SIZE);
	material.h = table.get_or("h", TILE_SIZE);
	material.l = table.get_or("l", 1);
	return material;
}

sol::table scripting::MaterialToLua(
		const content::Material &material, sol::this_state state) {
	sol::table table(state.L, sol::create);
	table["type"] = materialTypeToString(material.type);
	table["name"] = material.name;
	table["x"] = material.x;
	table["y"] = material.y;
	table["w"] = material.w;
	table["h"] = material.h;
	table["l"] = material.l;
	return table;
}
