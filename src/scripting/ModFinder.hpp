#pragma once

#include <vector>
#include <string>

#include "ModConfig.hpp"

namespace scripting {

/// This class is used to create an unsort a list of mods
/// by searching the mod install locations.
class ModFinder {
	std::vector<ModConfig> mods;

public:
	/// Adds all mods in path
	void addModsInPath(const std::string &dir);

	/// @returns Unsorted list of mod paths
	std::vector<ModConfig> getMods() const { return mods; }
};

} // namespace scripting
