#include "LuaDebugger.hpp"
#include "types/string.hpp"

#include <iostream>

#include <stdio.h>

#ifndef _WIN32
#	include <unistd.h>
#endif

using namespace scripting;

static thread_local LuaDebugger *instance = nullptr;
void LuaDebugger::trampoline(lua_State *L, lua_Debug *ar) {
	if (instance) {
		instance->onLineExecuted(ar);
	}
}

bool LuaDebugger::isInteractive() const {
#ifdef _WIN32
	return false;
#else
	return isatty(fileno(stdin));
#endif
}

void LuaDebugger::addBreakpoint(const std::string &file, int line) {
	breakpoints.emplace_back(file, line);
	attachHook();
}

void LuaDebugger::addBreakpoint(const std::string &str) {
	breakpoints.push_back(parseBreakpoint(str));
	attachHook();
}

void LuaDebugger::pause(lua_Debug *ar) {
	if (!isInteractive()) {
		return;
	}

	std::string last;
	while (true) {
		std::cout << "> ";
		std::flush(std::cout);

		std::string line;
		std::getline(std::cin, line);

		if (line.empty()) {
			line = last;
		}

		if (line.empty()) {
			std::cout << "Type a Lua script, or .help" << line << std::endl;
			continue;
		}

		last = "";

		if (line[0] != '.') {
			if (runLuaAtScope(ar, line)) {
				last = line;
			}
			continue;
		}

		if (line == ".help") {
			std::cout << "Commands: .continue (.c), .breakpoint (.b)"
					  << std::endl;
		} else if (line == ".c" || line == ".continue" || line == ".quit") {
			stepping = false;
			break;
		} else if (line == ".s" || line == ".step") {
			stepping = true;
			attachHook();
			break;
		} else if (line.substr(0, 2) == ".b") {
			Breakpoint bp = parseBreakpoint(line.substr(line.find(' ') + 1));
			if (bp.valid()) {
				std::cout << "Set breakpoint at " << bp.file << " line "
						  << bp.line << std::endl;
				breakpoints.push_back(bp);
			} else {
				std::cout << "Invalid breakpoint. Usage: .b "
							 "path/to/file.lua:123"
						  << std::endl;
			}
		} else {
			std::cout << "Unknown command: " << line << std::endl;
		}
	}
}

void LuaDebugger::onLineExecuted(lua_Debug *ar) {
	if (lua_getinfo(lua, "Sl", ar) == 0) {
		LogF(ERROR, "Error getting debug information");
		return;
	}

	if (stepping && ar->short_src) {
		LogF(INFO, "Step: %s:%d", ar->short_src, ar->currentline);
		pause(ar);
	} else if (ar->currentline > 0 && ar->source && ar->source[0] == '@') {
		// Check breakpoints
		std::string filepath = &ar->source[1];
		for (const auto &bp : breakpoints) {
			if (bp.line == ar->currentline && bp.file == filepath) {
				LogF(INFO, "Hit breakpoint! %s:%d", filepath.c_str(),
						ar->currentline);
				pause(ar);
				break;
			}
		}
	}
}

Breakpoint LuaDebugger::parseBreakpoint(const std::string &str) {
	auto colon = str.find(':');
	if (colon == std::string::npos) {
		return {"", -1};
	}

	std::string file = str.substr(0, colon);
	trim(file);

	std::string line = str.substr(colon + 1);
	int lineno = stoi(line);

	return {file, lineno};
}

void LuaDebugger::attachHook() {
	if (!instance && isInteractive()) {
		lua_sethook(lua, &LuaDebugger::trampoline, LUA_MASKLINE, 0);

		instance = this;
	}
}

bool LuaDebugger::runLuaAtScope(lua_Debug *ar, const std::string &line) {
	sol::environment env;
	if (ar && lua_getinfo(lua, "uf", ar) != 0) {
		sol::function func = sol::stack::get<sol::function>(lua, -1);
		sol::environment funcEnv = sol::get_environment(func);
		env = sol::environment(lua, sol::create, funcEnv);
	} else {
		env = sol::environment(lua, sol::create, lua.globals());
	}

	// First try printing
	sol::load_result script = lua.load("print(" + line + ")");
	if (!script.valid()) {
		// Next try plain Lua
		script = lua.load(line);
		if (!script.valid()) {
			sol::error err = script;
			std::cout << "Syntax error: " << err.what() << std::endl;
			return false;
		}
	}

	// Set f env
	env.set_on((sol::protected_function)script);

	if (ar) {
		for (int n = 1;; n++) {
			const char *name = lua_getlocal(lua, ar, n);
			if (name == NULL) {
				break;
			}

			env[name] = sol::stack::pop<sol::object>(lua);
		}
	}

	sol::protected_function_result result = script();
	if (!result.valid()) {
		sol::error err = script;
		std::cout << "Error: " << err.what() << std::endl;
		return false;
	}

	return true;
}
