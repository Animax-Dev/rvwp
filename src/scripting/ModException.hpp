#pragma once

#include <exception>
#include <string>

namespace scripting {

class ModException : public std::exception {
public:
	std::string msg;

	explicit ModException(const std::string &msg) : msg(msg) {}

	const char *what() const noexcept override { return msg.c_str(); }
};

} // namespace scripting
