#pragma once

#include "Lua.hpp"

namespace scripting {

sol::object l_parse_json(const std::string &value, sol::this_state state);
sol::object l_read_json(const std::string &path, sol::this_state state);

} // namespace scripting
