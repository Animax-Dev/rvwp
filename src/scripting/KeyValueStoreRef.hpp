#pragma once

#include <memory>
#include <utility>

#include "content/KeyValueStore.hpp"
#include "Lua.hpp"

namespace scripting {

class KeyValueStoreRef {
	std::weak_ptr<content::KeyValueStore> store;

	std::shared_ptr<content::KeyValueStore> getReference() const {
		if (store.expired()) {
			throw sol::error("Accessed expired KeyValueRef");
		}

		return store.lock();
	}

	sol::optional<std::string> get_string(const std::string &key) const;
	void set_string(const std::string &key, const std::string &value);

	sol::optional<int> get_int(const std::string &key) const;
	void set_int(const std::string &key, int value);

	sol::optional<bool> get_bool(const std::string &key) const;
	void set_bool(const std::string &key, bool value);

	sol::optional<float> get_float(const std::string &key) const;
	void set_float(const std::string &key, float value);

	sol::table to_table(sol::this_state state);

public:
	explicit KeyValueStoreRef(std::weak_ptr<content::KeyValueStore> store)
			: store(std::move(store)) {}

	static void registerToNamespace(sol::state &lua);
};

} // namespace scripting
