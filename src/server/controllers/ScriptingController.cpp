#include "../../world/AStarPathfinder.hpp"
#include "ScriptingController.hpp"
#include "../scripting/LuaScripting.hpp"
#include "server/Server.hpp"

#include "../../scripting/ModFinder.hpp"
#include "../../scripting/ModOrderResolver.hpp"
#include "../../scripting/ModException.hpp"

using namespace server;
using namespace world;
using namespace scripting;

void ScriptingController::recreate() {
	scripting = std::make_unique<LuaScripting>(this);
	scripting->init(ScriptingInterface::Domain::CHUNK);

	ModFinder finder{};
	finder.addModsInPath("mods");
	ModOrderResolver resolver(finder.getMods());
	std::vector<ModConfig> mods = resolver.resolve();
	for (const auto &mod : mods) {
		scripting->initMod(mod);
	}
}

void ScriptingController::update(float dtime) {
	if (scripting->getIsDead()) {
		if (retryCounter > 5) {
			throw ModException("Lua environment died, reached max_tries");
		}

		retryCounter++;
		server->sendChatMessageToAllPlayers("** Resetting Lua **");
		recreate();
	} else if (retryCounter > 0) {
		retryCounter--;
	}
}

world::World *ScriptingController::getWorld() {
	return server->getWorld();
}

world::WorldChunk *ScriptingController::getChunk(const V3s &cpos) {
	return server->getWorld()->getChunk(cpos);
}

world::WorldTile *ScriptingController::getTile(
		const V3s &pos, world::ChunkLayer layer) {
	return server->getWorld()->get(pos, layer);
}

bool ScriptingController::setTile(
		const V3s &pos, world::ChunkLayer layer, const world::WorldTile &tile) {
	if (!server->getWorld()->set(pos, layer, tile)) {
		return false;
	}

	auto chunk = server->getWorld()->getChunk(PosToCPos(pos));
	if (chunk) {
		chunk->dirty = true;
	}

	return true;
}

content::DefinitionManager *ScriptingController::getDefinitionManager() {
	return server->getDefinitionManager();
}

content::ItemDef *ScriptingController::getItemDefinition(
		const std::string &name) {
	return server->getDefinitionManager()->getItemDef(name);
}

content::TileDef *ScriptingController::getTileDefinition(
		const std::string &name) {
	return server->getDefinitionManager()->getTileDef(name);
}

content::TileDef *ScriptingController::getTileDefinition(content_id id) {
	return server->getDefinitionManager()->getTileDef(id);
}

Entity *ScriptingController::getPlayerEntity(const std::string &name) {
	auto peer = server->findPeerByName(name);
	if (!peer) {
		return nullptr;
	}

	return peer->entity;
}

void ScriptingController::getEntitiesInRange(
		const V3f &pos, std::vector<Entity *> &entities, float range) {
	return server->getWorld()->getEntitiesInRange(pos, entities, range);
}

Entity *ScriptingController::spawnEntity(
		const V3f &pos, const std::string &entityType) {
	World *world = server->getWorld();
	auto entity = world->createEntity(
			pos, entityType, content::Material("sword.png"));
	createEntity(entity);

	return entity;
}

bool ScriptingController::loadChunksAroundPlayer(
		const std::string &name, const int width, const int max_emerge) {
	auto peer = server->findPeerByName(name);
	if (peer) {
		server->loadChunksAroundPlayer(peer, width, max_emerge);
		return true;
	}

	return false;
}

world::Plot *ScriptingController::getPlot(int id) {
	return server->getWorld()->getPlot(id);
}

world::Plot *ScriptingController::getPlot(const V3s &pos) {
	return server->getWorld()->getPlot(pos);
}

world::Plot *ScriptingController::createPlot(
		const std::string &name, const Rect3s &bounds) {
	return server->getWorld()->createPlot(name, bounds);
}

bool ScriptingController::removeWork(u32 workId) {
	return server->getWorld()->removeWork(workId);
}

bool ScriptingController::sendChatMessage(
		const std::string &playerName, const std::string &message) {
	return server->sendChatMessageToPlayer(playerName, message);
}

void ScriptingController::sendChatMessage(const std::string &message) {
	server->sendChatMessageToAllPlayers(message);
}

bool ScriptingController::findPath(std::vector<V3s> &path, const V3s &from,
		const V3s &to, const Pathfinder::Settings &settings) {
	AStarPathfinder pathfinder(server->getWorld());
	return pathfinder.findPath(path, from, to, settings);
}

std::optional<std::shared_ptr<content::KeyValueStore>>
ScriptingController::getMetaData(const V3s &pos, ChunkLayer layer) {
	return server->getWorld()->getMetaData(pos, layer);
}

void ScriptingController::findNodesWithMetaData(
		std::vector<std::pair<V3s, WorldTile *>> &result, const V3f &pos,
		float range, ChunkLayer layer, const std::string &metadata) {
	return server->getWorld()->findNodesWithMetaData(
			result, pos, range, layer, metadata);
}

bool ScriptingController::pushTileTimer(const world::TileTimer &timer) {
	return server->getWorld()->pushTimer(timer);
}

const GameTime &ScriptingController::getGameTime() {
	return server->getGameTime();
}

void ScriptingController::setTimeOfDay(float timeOfDay, float speed) {
	server->setTimeOfDay(timeOfDay, speed);
}

std::optional<int> ScriptingController::getSurfaceHeight(V3s pos) {
	return server->getWorld()->getSurfaceHeight(pos);
}

ServerPlayer *ScriptingController::getPlayer(const std::string &username) {
	return server->getPlayerInfo(username);
}

server::Peer *ScriptingController::getPeer(const std::string &username) {
	return server->findPeerByName(username);
}
