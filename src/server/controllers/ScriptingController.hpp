#pragma once

#include "types/types.hpp"
#include "types/Controller.hpp"
#include "world/World.hpp"
#include "world/GameTime.hpp"
#include "world/Pathfinder.hpp"
#include "server/scripting/ScriptingInterface.hpp"

#include <memory>
#include <server/models/ServerPlayer.hpp>
#include <server/models/Peer.hpp>

namespace server {

class Server;
class ScriptingController : public Controller {
	std::unique_ptr<scripting::ScriptingInterface> scripting;
	server::Server *server;
	int retryCounter = 0;

public:
	explicit ScriptingController(Server *server) : server(server) {}

	//
	// Server API
	//

	void recreate();
	void update(float dtime) override;

	void onChatMessage(const std::string &name, const std::string &message) {
		scripting->onChatMessage(name, message);
	}

	void runExpiredTimers(const std::vector<world::TileTimer> &timers) {
		scripting->runExpiredTimers(timers);
	}

	bool createEntity(world::Entity *entity) {
		return scripting->createEntity(entity);
	}

	bool updateEntity(world::Entity *entity, float dtime) {
		return scripting->updateEntity(entity, dtime);
	}

	bool tileInteract(world::Entity *entity, const V3s &pos) {
		return scripting->tileInteract(entity, pos);
	}

	//
	// Scripting API
	//

	world::World *getWorld();
	world::WorldChunk *getChunk(const V3s &cpos);
	world::WorldTile *getTile(const V3s &pos, world::ChunkLayer layer);
	[[nodiscard]] bool setTile(const V3s &pos, world::ChunkLayer layer,
			const world::WorldTile &tile);

	content::DefinitionManager *getDefinitionManager();
	content::ItemDef *getItemDefinition(const std::string &name);
	content::TileDef *getTileDefinition(const std::string &name);
	content::TileDef *getTileDefinition(content_id id);

	world::Entity *getPlayerEntity(const std::string &name);

	void getEntitiesInRange(const V3f &pos,
			std::vector<world::Entity *> &entities, float range);

	[[nodiscard]] world::Entity *spawnEntity(
			const V3f &pos, const std::string &entityType);

	[[nodiscard]] bool loadChunksAroundPlayer(
			const std::string &name, int width = 9, int max_emerge = 10);

	world::Plot *getPlot(int id);
	world::Plot *getPlot(const V3s &pos);
	world::Plot *createPlot(const std::string &name, const Rect3s &bounds);
	bool removeWork(u32 workId);

	[[nodiscard]] bool sendChatMessage(
			const std::string &playerName, const std::string &message);

	void sendChatMessage(const std::string &message);

	[[nodiscard]] bool findPath(std::vector<V3s> &path, const V3s &from,
			const V3s &to, const world::Pathfinder::Settings &settings);

	std::optional<std::shared_ptr<content::KeyValueStore>> getMetaData(
			const V3s &pos, world::ChunkLayer layer);

	void findNodesWithMetaData(
			std::vector<std::pair<V3s, world::WorldTile *>> &result,
			const V3f &pos, float range, world::ChunkLayer layer,
			const std::string &metadata);

	[[nodiscard]] bool pushTileTimer(const world::TileTimer &timer);

	const world::GameTime &getGameTime();
	void setTimeOfDay(float timeOfDay, float speed = -1);
	std::optional<int> getSurfaceHeight(V3s pos);
	ServerPlayer *getPlayer(const std::string &username);
	server::Peer *getPeer(const std::string &username);
};

} // namespace server
