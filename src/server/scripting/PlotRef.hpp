#pragma once

#include "world/Plot.hpp"
#include "scripting/Lua.hpp"

namespace server {
class ScriptingController;
}

namespace scripting {

class PlotRef {
	server::ScriptingController *controller;
	world::Plot *plot;

	int get_id() const;
	std::string get_name() const;
	sol::table get_bounds(sol::this_state state) const;
	bool contains(V3s pos) const;
	sol::table get_boxes(sol::this_state state) const;
	void add_box(sol::table box);
	void add_boxes(sol::table boxes);
	void set_boxes(sol::table boxes);
	sol::table get_work(sol::this_state state) const;
	bool remove_work(const sol::object &obj);

public:
	explicit PlotRef(server::ScriptingController *controller, world::Plot *plot)
			: controller(controller), plot(plot) {}
	static void registerToNamespace(sol::state &lua);
};

} // namespace scripting
