#pragma once

#include "world/World.hpp"
#include "scripting/Lua.hpp"
#include "server/controllers/ScriptingController.hpp"

namespace scripting {

class TileTimerRef {
	server::ScriptingController *scriptingCtr;
	V3s position;
	world::ChunkLayer layer;
	std::string event;

	bool start(float delay);

public:
	explicit TileTimerRef(server::ScriptingController *scriptingCtr,
			V3s position, world::ChunkLayer layer, const std::string &event)
			: scriptingCtr(scriptingCtr), position(position), layer(layer),
			  event(event) {}

	static void registerToNamespace(sol::state &lua);
};

} // namespace scripting
