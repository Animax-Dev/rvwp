#include "PlayerRef.hpp"
#include <sstream>

using namespace scripting;

void PlayerRef::registerToNamespace(sol::state &lua) {
	auto equal_to = sol::meta_function::equal_to;

	// clang-format off
	lua.new_usertype<PlayerRef>("PlayerRef",
			equal_to,               &PlayerRef::equals,
			"get_username",         &PlayerRef::get_username,
			"is_online",            &PlayerRef::is_online,
			"get_entity",           &PlayerRef::get_entity,
			"get_address",          &PlayerRef::get_address);
	// clang-format on
}

bool PlayerRef::equals(const PlayerRef &L) const {
	return player == L.player;
}

std::string PlayerRef::get_username() const {
	return player->username;
}

bool PlayerRef::is_online() const {
	return scriptingController->getPeer(player->username) != nullptr;
}

sol::optional<EntityRef> PlayerRef::get_entity() const {
	auto entity = scriptingController->getPlayerEntity(player->username);
	if (!entity) {
		return {};
	}

	return EntityRef(scriptingController->getPlayerEntity(player->username));
}

sol::table PlayerRef::get_address(sol::this_state state) const {
	auto peer = scriptingController->getPeer(player->username);

	sol::table table(state.L, sol::create);
	table["ip"] = peer->address.getAddressString();
	table["port"] = peer->address.GetPort();
	return table;
}
