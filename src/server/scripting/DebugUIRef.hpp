#pragma once

#include "scripting/Lua.hpp"

namespace scripting {

class DebugUIRef {
	bool is_enabled() const;
	void draw_label(V3f pos, const std::string &text);
	void draw_line(V3f from, V3f to, const std::string &color);

public:
	static void registerToNamespace(sol::state &lua);
	static bool isAvailable();
};

} // namespace scripting
