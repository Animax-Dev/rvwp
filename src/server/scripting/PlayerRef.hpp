#pragma once

#include "server/controllers/ScriptingController.hpp"
#include "server/models/ServerPlayer.hpp"
#include "scripting/Lua.hpp"
#include "EntityRef.hpp"

namespace scripting {

class PlayerRef {
	server::ServerPlayer *player;

	server::ScriptingController *scriptingController;
	bool equals(const PlayerRef &L) const;
	std::string get_username() const;
	bool is_online() const;
	sol::optional<EntityRef> get_entity() const;
	sol::table get_address(sol::this_state state) const;

public:
	PlayerRef(server::ServerPlayer *player,
			server::ScriptingController *scriptingController)
			: player(player), scriptingController(scriptingController) {}
	static void registerToNamespace(sol::state &lua);
};

} // namespace scripting
