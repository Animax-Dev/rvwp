#pragma once
#include "../network/protocol.hpp"
#include "../network/packets.hpp"
#include "../network/net.hpp"
#include "../content/content.hpp"
#include "content/Inventory.hpp"
#include "world/World.hpp"
#include "server/models/ServerPlayer.hpp"
#include "server/models/Peer.hpp"
#include "controllers/WorldController.hpp"
#include "controllers/ScriptingController.hpp"
#include "scripting/LuaScripting.hpp"
#include "ServerSpec.hpp"
#include <string>
#include <map>

namespace server {

class Server {
	ServerSpec spec;
	network::Socket socket;
	std::map<network::Address, std::unique_ptr<Peer>> peers;
	std::map<std::string, std::unique_ptr<ServerPlayer>> player_infos;
	std::vector<Controller *> controllers;
	std::unique_ptr<WorldController> world_ctr;
	content::DefinitionManager definitionManager;
	ScriptingController scripting_ctr;

	world::GameTime time;

	/// Adds controller to controllers, causes update() to be called.
	void registerController(Controller *ctr) { controllers.push_back(ctr); }

public:
	explicit Server(const ServerSpec &spec);
	Server() : Server(ServerSpec()) {}
	Server(const Server &that) = delete;
	~Server();

	//
	// Server state
	//
	bool start(unsigned short port); ///< open socket
	bool run();						 ///< server update loop
	bool update(float dtime);		 ///< fixed time step
	void close();

	//
	// World / Environment
	//
	inline world::World *getWorld() { return world_ctr->getWorld(); }
	V3f getRespawnPosition(Peer *dead_player) { return V3f(1, 1, 0); }

	/// Resend entity location when the entity moves a significant distance
	void detectAndSendEntityChanges();

	/// Check around player for unloaded chunks, and send or emerge them
	void loadChunksAroundPlayer(Peer *peer, int width = 10, int max_emerge = 3);

	bool loadChunkForPlayer(
			Peer *peer, const V3s &cpos, world::WorldChunk **chunk = nullptr);

	content::DefinitionManager *getDefinitionManager() {
		return &definitionManager;
	}

	const world::GameTime &getGameTime() { return time; }

	//
	// Player/peer helpers
	//

	/// Get peer by name
	/// @return the peer
	inline Peer *getPeer(network::Address address) {
		auto it = peers.find(address);
		return (it == peers.end()) ? nullptr : it->second.get();
	}

	/// Looks for the `Peer` in `peers`.
	/// @return the peer
	Peer *findPeerByName(const std::string &name) const;

	/// Looks for the `Peer` in `peers`.
	/// @return the peer
	Peer *findPeerByEntity(const world::Entity *ent) const;

	/// May be asynchronous in the future, requests the emerge of a block
	void requestEmerge(const V3s &cpos);

	/// Creates an entity for a client, and adds them to the world
	bool emergePlayer(Peer *client, V3f position);

	/// Makes a player leave the world - not necessarily the game.
	bool leavePlayer(Peer *client);

	/// Print list of players to log
	/// @param brackets Whether to print out (List of players: ) too.
	void printPlayers(bool brackets = true);

	/// Get player info
	///
	/// @param name Username
	/// @param create Whether to create if it does not exist
	/// @return ServerPlayer
	ServerPlayer *getPlayerInfo(const std::string &name, bool create = false);

	void setTimeOfDay(float timeOfDay, float speed = -1);

	//
	// Packets and Networking
	//

	void initPacketHandlers();

	void handlePlayerConnect(network::Packet &pkt);
	void handlePlayerDisconnect(network::Packet &pkt);
	void handlePlayerMove(network::Packet &pkt);
	void handlePlayerInventoryCommand(network::Packet &pkt);
	void handleChatMessage(network::Packet &pkt);
	void handleSetTile(network::Packet &pkt);
	bool doSetTileNoRollback(
			Peer *peer, const V3s &pos, u32 cid, u8 hotbar_id, u16 hp);
	void handleTileInteract(network::Packet &pkt);
	void handlePunchEntity(network::Packet &pkt);
	void handleWorldNewWork(network::Packet &pkt);
	void handleWorldRemoveWork(network::Packet &pkt);
	void handleFireWeapon(network::Packet &pkt);

	bool sendChatMessageToPlayer(
			const std::string &playerName, const std::string &message) {
		Peer *peer = findPeerByName(playerName);
		if (!peer) {
			return false;
		}

		sendChatMessageToPlayer(peer, message);
		return true;
	}

	/// Disconnects an address
	/// If there is an associated peer,
	/// leavePlayer() will be called and the peer will be removed
	void sendDisconnectAndRemovePeer(network::Address address,
			const std::string &errm, int reconnect = 1);

	void sendPlayerProfile(Peer *peer, ServerPlayer *player = nullptr);
	void sendPlayerInventory(Peer *peer);
	void sendDefinitions(Peer *peer);
	void sendChatMessageToPlayer(Peer *peer, const std::string &message);
	void sendChatMessageToAllPlayers(
			const std::string &message, Peer *ignore_peer = nullptr);

	void sendGameTimeToPlayer(Peer *peer, world::GameTime gameTime);
	void sendChunkToPlayer(Peer *peer, world::WorldChunk *chunk);
	void sendSetTileToAllPlayers(const V3s &pos, world::ChunkLayer layer,
			Peer *ignore_peer = nullptr);
	void sendPlayerPosition(Peer *peer);
	void sendRemoveEntityToPlayer(Peer *peer, int id);
	void sendRemoveEntityToAllPlayers(int id, Peer *exclude = nullptr);
	void sendDirtyChunkToWatchers(world::WorldChunk *chunk);
	void sendPlotToPlayer(Peer *peer, const world::Plot *plot);
	void sendWorkCreatedToPlayer(
			Peer *peer, int plotId, const world::Work *work);
	void sendWorkCreatedToWatchers(int plotId, const world::Work *work);
	void sendWorkRemovedToPlayer(Peer *peer, u32 serverId);
	void sendWorkRemovedToWatchers(u32 plotId, u32 workId);
	void sendSpawnParticle(Peer *peer, const V3f &position, const V3f &velocity,
			const content::Material &material);
	void sendSpawnParticleToAllPlayers(const V3f &position, const V3f &velocity,
			const content::Material &material, Peer *exclude);
};

} // namespace server
