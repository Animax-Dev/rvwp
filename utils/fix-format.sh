#!/bin/bash

find src/ -path src/libs -prune -o \( -name '*.cpp' -or -name '*.hpp' \) -type f -printf '%p\n' -exec clang-format -i {} \;
