# Ruben's Virtual World Project

Hybrid topdown shooter and basebuilder, set in a modern dystopian environment

See [the website](https://rubenwardy.com/rvwp/) and docs/ for more info.

## License

All Rights Reserved. You may try out this game, but may not redistribute,
modify, or make commercial use of.

## How to play

Open the executable in the bin folder.

Controls:

* Move: WASD
* Camera slice: PgUp / PgDn
* Switch mode: F7
* Hero mode:
	* Use item: Left/right-click
	* Jump: Space
	* Sneak: Shift
	* Inventory: Tab
* Architect mode:
	* Use tool: Left-click
	* Release tool: Right-click

