---
title: Behaviour
layout: default
nav_order: 10
---

# Behaviour

Behaviour trees are used to compose complex entity behaviour from
simple reusable nodes.

The behaviour tree system is defined in `mods/core/behaviour`

## Classes

All these classes will exist in the `behaviour` global table.

* BehaviourTree - the root node.
* `Node` - base node, defines `run()` and `get_state()`.
* Branches:
    * Selector - tries each child in turn until one succeeds.
    * FickleSelector - tries each child _on each turn_, until one succeeds.
* Sequences:
    * Sequence - runs children in turn, stopping on failure.
    * FickleSequence - runs all children _on each turn_, stopping on failure.
