---
title: EntityController
layout: default
nav_order: 10
---

# EntityController

An EntityController is something that drives an [Entity](EntityRef.html)'s behaviour.
An Entity has at most two controllers - a primary controller, and an action controller.
The action controller is a temporary controller used to perform an active task such as move to a certain
location.

* `EntityController:new()`
* `entity` - the [EntityRef](#EntityRef). Set by the engine.
* `get_action(self)` - gets the current action controller, or nil
* `set_action(self, action_ctr)` - set the current action. See [rvwp.entity.actions](actions.md). 

Override recommended:

* `on_create(self)` - entity has been created and emerged
* `step(self, dtime)` - dtime in seconds


Example:

```lua
local Thing = EntityController:new()

function Thing:on_create()
    self.entity:set_properties({ material = "sword.png" })
end

function Thing:step(dtime)
    local pos = Vector:new(self.entity:get_pos())
    local delta = pos:copy():normalize():multiply(1 * dtime)
    self.entity:set_pos(pos:add(delta))
end

rvwp.register_entity("thing", Thing)
```

with sub-controllers:

```lua
local Follower = EntityController:new()
Follower.texture = "sword.png"

function Follower:on_create() 
    self.entity:set_properties({ material = "sword.png" })
    self.actions:go_to(Vector:new(3, 4, 5))
end

rvwp.register_entity("follower", Follower)
```
