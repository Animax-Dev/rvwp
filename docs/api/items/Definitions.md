---
title: Definitions
layout: default
nav_order: 10
---

# Definitions

## Item Definitions

See [Materials](Materials.html).

```lua
rvwp.register_item("name", {
    title = "Human readable title",
    material = {
        x = 3, y = 4,
    },
})
```

## Weapon Definition 

```lua
rvwp.register_item("pistol", {
    title = "Pistol",
    material = "pistol.png",

    -- Weapon specification
    weapon = {
        -- melee or gun
        type = "gun",
    
        -- Maximum range
        range = 10,

        -- Base damage
        damage = 10,
        
        -- Time between uses
        use_interval = 1,
    },    
})
```

## Tile Definitions

```lua
rvwp.register_tile("glow_stone", {
    title = "Glow Stone",
    material = {
        x = 4, y = 0,
    },
    
    -- Can be built by the player
    buildable = true,

    -- Whether entities collide
    collides = true,
    
    -- Light source value, 0-15 / 0x0-0xF
    light_source = 0xC,

    -- Whether light propagates
    light_propagates = true,
    
    -- An entity has attempted to interact (E) with this tile.
    --
    -- Return false if the interaction wasn't handled.
    -- Returning nil or nothing is the same as returning true.
    on_interact = function(pos, layer, tile, entity)
        print("Interact!")
    end,
})
```

## Floor / Terrain Definitions

Extends tile definition.

```lua
rvwp.register_floor("floor_wood", {
    title = "Wooden Floor",
    material = {
        x = 1, y = 1,
    },
    footstep_sound = "wood",

    -- dampener = 1 - friction
    -- Must not exceed 1. <0 will increase speed unrealistically.
    friction = 0.2,

    buildable = true,
})
```

## Tool Definitions

```lua
rvwp.register_tool({
    -- cancel or build
    type = "build",

    description = "Build Something",
    material = { x = 2, y = 3 },
    item_name = "wall",
    select_type = "filled",
    
    -- if select_type is "fixed"
    fixed_selection_size = V(1, 3, 1),
})
```
