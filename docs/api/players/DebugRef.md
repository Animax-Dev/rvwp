---
title: DebugUIRef
layout: default
nav_order: 10
---

# DebugUIRef

A DebugUIRef is used to create debug annotations on the world.

* `is_enabled()` - true if the debug UI is currently enabled.
* `draw_label(pos, text)` - Adds a 3D label.
* `draw_line(from, to, color)` - Adds a 3D line. Color is a hex string.
