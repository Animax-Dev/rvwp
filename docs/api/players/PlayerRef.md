---
title: PlayerRef
layout: default
nav_order: 10
---

# PlayerRef

A player may be online or offline.

* `get_username()`
* `is_online()` - Returns true if the player is online.
* `get_entity()` - Returns [EntityRef](../entities/EntityRef.html) the player is controlling.
* `get_address()` - Returns table with:
    * `ip` - string
    * `port` - integer
