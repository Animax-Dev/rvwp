---
title: ChatCommand
layout: default
nav_order: 10
---

# ChatCommand

* `ChatCommand:new(x)` - `x` should be a table with functions to override.
* `cmd:execute(name, param)` - execute command for player `name` and `params`. Checks permissions.
* `cmd:func(name, params)`
    * Abstract, needs to be implemented.
    * Returns a tuple: `success, message`. If `message` is non-nil, it will be sent to the caller.

Example:

```lua
rvwp.register_chat_command("echo", ChatCommand:new({
    help = "<message>\n - message: will be repeated.",
    func = function(self, name, param)
        return true, param
    end
}))
```
