---
title: RVWP Namespace
layout: default
nav_order: 10
---

# RVWP Namespace

## General

* `rvwp.get_mods()` - Returns array of mod names.

## Registration

* `rvwp.register_item(name, def)` - See [Definitions](items/Definitions.html).
* `rvwp.register_tile(name, def)`
* `rvwp.register_floor(name, def)`
* `rvwp.register_terrain(name, def)`
* `rvwp.register_tool(def)`
* `rvwp.register_entity(name, entity)` - entity is a lua entity.
* `rwvp.register_chat_command(name, cmd)` - cmd is a ChatCommand instance.

## Environment

### Tiles

* `rvwp.register_tile(name, def)` - Registers a tile type
    * Event callbacks in the form `on_event`.
        * Tile timers `on_event = function(pos, layer, tile)` - See [TileTimerRef](items/TileTimerRef.html).
* `rvwp.get_layer(pos, layer)`
    * Returns a tile table.
    * `pos` is a 3D position.
    * `layer` may be:
        * `rvwp.layers.tile`
        * `rvwp.layers.floor`
        * `rvwp.layers.terrain`
* `rvwp.set_layer(pos, layer, tile)` - pos is a 3D position, and tile is a tile table. Returns true on success.
* `rvwp.get_tile(pos)` - pos is a 3D position. Returns a tile table.
* `rvwp.set_tile(pos, tile)` - pos is a 3D position, and tile is a tile table. Returns true on success.
* `rvwp.get_floor(pos)` - pos is a 3D position. Returns a tile table.
* `rvwp.set_floor(pos, tile)` - pos is a 3D position, and tile is a tile table. Returns true on success.
* `rvwp.get_terrain(pos)` - pos is a 3D position. Returns a tile table.
* `rvwp.set_terrain(pos, tile)` - pos is a 3D position, and tile is a tile table. Returns true on success.
* `rvwp.get_surface_height(pos)` - pos is a 2D position, returns the actual height of the surface or nil.
* `rvwp.get_meta(pos, layer)` - get meta for a layer, returns nil if the area isn't loaded or if there is nothing there.
* `rvwp.find_path(from, to, [settings])` - pathfinder, returns list of positions or nil.
    * Settings is an optional table with the following contents: 

        ```lua
        {
            -- Optional, the path cost before the pathfinder gives up.
            -- Cost is roughly 1 per tile, but jumping and falling cost extra.
            -- Positive values set an absolute cost,
            -- negative values are a multiplier against the aerial distance.
            give_up_cost = -3,

            -- If true, the pathfinder will accept the exact end position.
            accept_exact_target = true,

            -- If true, the pathfinder will accept neighbours of the end position.
            accept_neighbours = false,

            -- If true, the pathfinder will accept the level below any otherwise
            -- accepted position.
            accept_below = false,

            -- Table of tile names to weights. Can be used to make certain tiles
            -- passable or impassable
            weights = {
                door = 3
            }
        }
        ```
* `rvwp.find_with_metadata(pos, range, layer, meta_key)`
    * Finds things with meta data.
    * Returns `positions, tiles`
* `rvwp.get_timer(pos, layer, event)` - see [TileTimerRef](items/TileTimerRef.html)
* `rvwp.remove_work(work_id)`

Tile table:

```lua
{
    name = "tile_name"
}
```

### Entities

* `rvwp.get_player_entity(name)` - gets the entity for a player, or nil
* `rvwp.get_entities_in_range(pos, range)` - returns a list of EntityRefs.
* `rvwp.spawn_entity(pos, type_name)` - create entity at position.

### Plots and Rooms

* `rvwp.get_plot(id)` - returns PlotRef or nil
* `rvwp.get_plot_at_pos(pos)` - returns PlotRef or nil
* `rvwp.create_plot(def)` - creates a plot, returns PlotRef or nil
    * `def.name` - human readable name
    * `def.from` - starting position of initial box
    * `def.size` - size of initial box

### Misc

* `rvwp.format_chat_message(name, message)` - returns formatted chat message.
* `rvwp.get_nick_color(name)` - get color string for a username.
* `rvwp.send_chunks_to_player(name, width, max)` - sends up to `max` of the `width*width` chunks
    surrounding player `name`.
* `rvwp.get_time()` - returns a table with the following keys and values:
    * `time_of_day` - decimal value in the range `0 <= x < 24`. Eg: 13.5 is 1.30pm.
    * `hours` - integer number of hours, 0-23.
    * `minutes` - integer number of minutes, 0-59.
    * `clock` - time of day as a locale-dependent string. Eg: 13:34.
    * `daylight` - current daylight, from 0-1.
    * `time_since_beginning` - seconds since the world was created.
    * `speed` - game seconds per real life seconds.
* `rvwp.set_time_of_day(time_of_day [, speed])` 

## Players

* `rvwp.get_player_entity(name)` - returns [PlayerRef](players/PlayerRef.html) if the player exists.
* `rvwp.execute_chat_command(name, cmd_name, params)` - returns tuple: `success, message`. Calling function is responsible for displaying `message`.
* `rvwp.chat_send_all(message)`
* `rvwp.chat_send_player(name, message)` - returns boolean, true on success

## Callbacks

* `rvwp.register_on_pre_chat_message(function(name, message))` - return true to stop message being sent to all players.
* `rvwp.register_on_chat_message(function(name, message))` - cannot be vetoed. Not called when the message is a command.

## JSON

* `rvwp.read_json(path)` - read JSON file to Lua.
* `rvwp.parse_json(serialize_string)` - read JSON string to Lua.

## Debug

These values will be nil if not available.

* `rvwp.debug` - A [DebugRef](players/DebugRef.html).
* `rvwp.debug_pause()` - Pause the debugger, halting script execution.
* `rvwp.debug_breakpoint(path, line)` - Add a breakpoint.
* `rvwp.debug_breakpoint(spec)` - Add a breakpoint, spec is in the form "path/to/file.lua:123".

## Color

All functions which accept a Color will also accept a 3 or 6 length
hex string like `#F00` or `#FF0000`.

TODO: Color class
